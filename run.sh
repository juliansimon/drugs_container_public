#!/bin/bash
cd ./Code/Propagation/
source compile.sh
cd -

if [ ! -f ./Code/Propagation/drugs_process ]; then
    echo "File ./Code/Propagation/drugs_process not found!"
    echo "Looks like compilation failed! Make sure you have g++ installed!"
    exit 1
fi

if [ ! -f ./Code/Propagation/decode_from_base64 ]; then
    echo "File ./Code/Propagation/decode_from_base64 not found!"
    echo "Looks like compilation failed! Make sure you have g++ installed!"
    exit 1
fi

echo "Preparing data for analysis"
python ./Code/prepare_data.py

echo "Propagating compound influences"
python ./Code/run_propagation.py

echo "Machine learning prediction"
python ./Code/run_machine_learning.py

echo "Run completed. If things went well, your results should be ready."