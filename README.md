# README #

DRUGS container is a unified collection of algorithms for modelling drug effects propagation through human interactome and predict anti-cancer likeness of the drug/food compound using machine learning approach and a set of known reference anti-cancer drugs (clinically approved). The purpose of this work is to highlight the best candidates for drug repositioning (i.e. non-anticancer approved drugs to be used as anti-cancer to speed up drug development) and also to detect potential novel anti-cancer compounds in known foods. This container is provided to allow readers of the corresponding paper (link here when available) to reproduce results presented or re-use the code for their own studies. The code is provided **AS IS** and is free for both commercial and non-commercial applications. The data provided is free for non-commercial use. Commercial use of the data may require obtaining corresponding licenses for the source databases.

##Installing and running:

The code in the container requires Linux-like environment to run with **g++** compiler installed and available from the command prompt. Although some steps of the process could work under Windows (python part), the actual gene propagation relies on the C++ code and its compilation by **g++** under Linux.  If you want to run the whole pipeline and you do not have a PC with Linux installed, we would recommend to install virtual environment, such as [**VirtualBox**](https://www.virtualbox.org/wiki/Downloads) for Windows and run Linux on the virtual machine. 

In addition to **g++** compiler, the code dependencies include python with [**NumPy**](http://www.numpy.org/), [**SciPy**](https://www.scipy.org/), [**Pandas**](https://pandas.pydata.org/), [**Scikit-learn**](https://scikit-learn.org/stable/) and [**Matplotlib**](https://matplotlib.org/) installed. We strongly recommend using [**Anaconda Python 3.6, 64 bit**](https://www.anaconda.com/distribution/), as it comes with many dependencies already pre-installed. You will also need a PC with large enough RAM, particularly if you intend to exclude outliers. This step can take up to 12 Gb of RAM. 

To run the code - simply download this container and unpack it in some folder to which you have writing/execution permissions and ~20 Gb of free space. Go to the container folder, adjust `./settings.ini` accordingly to set the running parameters (method, thresholds etc. See `./settings.ini` for the available options). The default one has the settings to generate the best model for the paper out of the 700 top ones and is ready to go. Run `./run.sh` in your bash terminal and this should be it. If dependencies are properly satisfied, this script should do all the needed steps and produce report and results in `./Results` folder in a few hours. `./Scratch` and `./Results` folders will be automatically created by the script. *Scratch* is a temporary folder for intermediate results. It can be deleted afterwards. *Results* will contain several logs and CSV files. CSV files will be prefixed with the machine learning method you used and will include: 
1. $METHOD_params_optimization.csv - table of ML statistics, i.e. F-score, % anti-cancer corrrect, % non-anticancer correct predictions
2. $METHOD_prob_train.csv and $METHOD_prob_test.csv - anti-cancer probability predicted for training and test sets of drugs/compounds
3. $METHOD_gene_scores.csv - correlation scores for each gene with the positive prediction of anti-cancer compounds (i.e. the higher the gene score - the more this gene is affected by the anti-cancer compounds) - available for MMC and LinearSVM predictors.
4. Optional CSV-s listing outlier compounds excluded from analysis if outlier detection was enabled in `./settings.ini`. 

##Re-using container

Should you wish to re-use the container to tackle your own problem of interest (*e.g.* predict anti-inflamatory compounds) - you can do so by modifying `./Compounds/compounds.csv.gz`.  It has 4 columns: Primary_ID, Label, Name and InChIKey. Label is set to 1 for anti-cancer (*i.e.* class 1) of the training set, 0 - for non-anticancer (i.e. class 0) in training set and any other value for test set. Test set will be predicted based on the model generated using training set. Change the labels according to your problem. Add/remove compounds if needed.  Just keep in mind that they will be matched to a mapped subset generated using STITCH via InChI key. If there is no match - the compound is ignored. If your compounds are not in the current list, you may need to modify `./Interactome/mapped.csv.gz` accordingly to add your compounds and their scored connections to genes in `./Interactome/genes.csv.gz`. You will need access to and a license for STITCH to do it (free for non-commercial use). 

###Legality and Disclaimers

The code provided is open source and is released under permissive MIT license (see LICENSE):

Copyright (c) 2019 Imperial College London

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

The data included was extracted from the following publicly available databases with corresponding licensing:

####Drug information (Name, InChI Key, ID, anticancer status label): 

1. [DrugBank](www.drugbank.ca) ("DrugBank is offered to the public as a freely available resource. Use and re-distribution of the data, in whole or in part, for commercial purposes (including internal use) requires a license.")
2. [RepoDB](http://apps.chiragjpgroup.org/repoDB/) (Creative Commons Attribution 4.0 International License)
3. Anticancer labels for drugs were obtained from [DrugCentral](http://drugcentral.org/) (Creative Commons BY SA license) 

####Food compounds (Name, InChI Key, ID):

1. [FooDB](http://foodb.ca/ ) ("FooDB is offered to the public as a freely available resource. Use and re-distribution of the data, in whole or in part, for commercial purposes requires explicit permission of the authors and explicit acknowledgment of the source material (FooDB).")

####Gene information, gene-gene and gene-compound connections:

1. [STRING](https://string-db.org/) (Creative Commons BY 4.0) - gene-gene connections are mainly derived from STRING.
2. [STITCH](http://stitch.embl.de/) ("For non-profit (academic) institutions, the STITCH license is free of charge. For commercial use, or for customized versions, please contact biobyte solutions GmbH.") - compound (InChI Key) - gene connections for the used drugs/food compounds were obtained from STITCH.
3. [UniProt](https://www.uniprot.org/) (Creative Commons BY 4.0) - Protein sequence for matching and gene/protein IDs.
4. [COSMIC](https://www.sanger.ac.uk/science/tools/cosmic) ("COSMIC is now under license, with academic and non-profit access available freely and commercial access available for a license fee") - Gene sequences and names used to match IDs between several gene information sources.
5. [Gene](https://www.ncbi.nlm.nih.gov/gene) - some additional IDs for genes extracted.
6. [BioPlex](http://bioplex.hms.harvard.edu/) - some additional gene-gene connections.

As some of the databases have more restrictive licensing regarding commercial use, you will need to check with them and, potentially, obtain their permission directly from them for any commercial use of the data extracted from their databases and included in this container. Non-commercial use should be OK as is - just do not forget to cite them in your paper/publication.

###Acknowledgments

Any papers describing data analytics using this package, or whose results were significantly aided by the use of this package (except when the use was internal to a larger program), should include an acknowledgment and citation to the following manuscript(s):

K. Veselkov, G. G. Pigorini, S. Aljifri, D. Galea, R. Mirnezami, J. Youssef, M. Bronstein and I. Laponogov.  HyperFoods: Machine intelligent mapping of cancer-beating molecules in foods (2019). To be published.

If the package is used prior to its open source release and publication, please contact [kirill.veselkov04@imperial.ac.uk](mailto:kirill.veselkov04@imperial.ac.uk) to ensure that all contributors are fairly acknowledged.

###Authors

Lead Developer: Dr. Ivan Laponogov [i.laponogov@imperial.ac.uk](mailto:i.laponogov@imperial.ac.uk) 
Chief project investigator: Dr. Kirill Veselkov [kirill.veselkov04@imperial.ac.uk](mailto:kirill.veselkov04@imperial.ac.uk)
Copyright 2019 Imperial College London

###Issues

Please report any bugs or requests that you have using the Bitbucket issue tracker!

###Development

This container is mainly provided to allow readers to reproduce results presented in the paper. We do not have solid plans for developing it further at the moment. You are welcome to use it for your own purposes. However, if you find it useful and would like to see it turned into a full-blown project/software package - please express your interest to the authors and we will see what we can do. 

###License:

The code is released under permissive MIT license. See LICENSE document in the root folder of this package.

###Funding:

Vodafone Foundation, UK

