# -*- coding: utf-8 -*-
"""
Created on Wed Jul 26 12:48:32 2017

Implementation of tic/toc functionality for python

--------------------------------------------------------------------------

Lead Developer: Dr. Ivan Laponogov (mailto:i.laponogov@imperial.ac.uk) 

Chief project investigator: Dr. Kirill Veselkov (mailto:kirill.veselkov04@imperial.ac.uk)

License: 

The MIT License (MIT)

Copyright 2019 Imperial College London

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

"""
#===========================Import section=================================

#Importing standard and external modules
import time;
import os;
import sys;

#If run independently - check system endianness and add path to local modules
if __name__ == "__main__": 
    if sys.byteorder!='little':
        print('Only little endian machines currently supported! bye bye ....');
        quit();
    module_path = os.path.abspath('%s/../..'%os.path.dirname(os.path.realpath(__file__)));
    sys.path.insert(0, module_path)

#Import local/internal modules
from printlog import printlog;

#==========================================================================
#From here the main code starts


__dttime = {'default':time.time()};

def tic(id_tic = 'default'):
    """
    Records the time it was called thus starting timing.
    
    Args:
        id_tic: string or number to be used to identify this starting point. 
        Default: 'default'
    
    """
    global __dttime;
    __dttime[id_tic] = time.time();
    
def toc(id_tic = 'default', printing = True):
    """
    Returns and prints (optionally) the number of seconds that passed since 
    corresponding tic(id) was called.
    
    Args:
        id_tic:   string or number to be used to identify corresponding starting
                  point. Default: 'default'.
        printing: boolean, determines if the number of elapsed seconds is 
                  printed. Default: True
                 
    Returns:
        number of seconds that passed since corresponding tic(id) was called as
        float.
    """
    global __dttime;

    try:
        t = time.time() - __dttime[id_tic];
    except:
        printlog('tic "%s" not found! Did you forget to call tic(%s)?'%(id_tic, id_tic));
        return 0.0;
    
    if printing:
        printlog('%s seconds'%t)
    return t;
    
    
    
if __name__ == "__main__": 
    tic(1);
    time.sleep(1);
    toc();tic('2')
    time.sleep(1);
    toc(1);
    toc('2');
    toc(2)