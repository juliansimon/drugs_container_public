"""
DRUGS Project module: run_propagation.py

Created on Mon Jan 15 11:52:26 2018

This is the assistive module which needs to be run after prepare_data.py
Its job is to check if the previous step created necessary input files and then
call C++ propagation routines normally used for DRUGS project propagation 
analysis within DreamLab App. 

To run it:

python run_propagation.py    

Doing propagation using separate C++ code speeds up the processing dramatically
compared to the python implementation counter-parts. So, at the moment we do not
support/provide python alternative to it. We also benefit from the C++ code already 
previously developed to run on the grid of smartphones (DreamLab) which we designed
for the maximum efficiency and versatility. This container uses only the part
of the code doing the full propagation and returning full vectors. PageRank
algorithm is the only currently supported by this container. Other algorithms and
in-smartphone correlational analysis of propagated profiles are not currently
accessible from this module, although available in C++ code. They are currently 
reserved for execution within DreamLab App. Discussing those falls outside of 
the scope of this reported study and will be covered in future releases. 

By default, the C++ code will be compiled automatically
as part of running this module. Thus, g++ compiler needs to be available via
command line as well as bash shell environment to run the compilation script. 
So, this module should be executed in Linux environment with
g++ compiler installed. Mac may work (untested), but Windows normally will not.
Other than that, it relies on Pandas library available in you python environment.

After (hopefully) successful propagation run, this module will decode and convert
the output propagated profiles for the subsequent use in machine learning step.


--------------------------------------------------------------------------

Lead Developer: Dr. Ivan Laponogov (mailto:i.laponogov@imperial.ac.uk) 

Chief project investigator: Dr. Kirill Veselkov (mailto:kirill.veselkov04@imperial.ac.uk)

License: 

The MIT License (MIT)

Copyright 2019 Imperial College London

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

"""

#Loading global modules and dependencies
import os;
import sys;
import subprocess
import numpy as np;
import hashlib;
import pandas as pd;

#Checking if the system is right and if the module is run as a script instead of being imported
if __name__== '__main__':
    if sys.byteorder!='little':
        print('Only little endian machines currently supported! bye bye ....');
        quit();
else:
    print('This module is not meant to be imported, but should be run as a script instead!')
    quit();
    
#Figuring out and adding the path to local modules    
module_path = os.path.dirname(os.path.realpath(__file__));
sys.path.append(module_path);

from printlog import start_log, stop_log, printlog;  #print and log printed messages in one go
#different functions used by the algorithm
from utils import load_global_settings; 


def getlen(arr):
    if isinstance(arr, np.ndarray):
        return len(arr)
    else:
        return 1;

######################################################################################
#Loading methods for different data types returned from propagator
######################################################################################

buff_position = 0;

def load_byte(buff):
    global buff_position;
    value = buff[buff_position];
    buff_position += 1;
    return value;
    
def load_int(buff, long_add):
    global buff_position;
    if long_add:
        value = (int(buff[buff_position])) | \
                (int(buff[buff_position + 1]) << 8)  |  \
                (int(buff[buff_position + 2]) << 16) | \
                (int(buff[buff_position + 3]) << 24); 
        buff_position += 4;
    else:
        value = (int(buff[buff_position])) | \
                (int(buff[buff_position + 1]) << 8);
        buff_position += 2;
    return value;

def load_selection(buff, count):
    global buff_position;
    sel_length = count // 8;
    if count % 8 != 0:
        sel_length += 1;
        
    sel = [0] * count;
    for i in range(count):
        sel[i] = (int(buff[buff_position + i//8]) & (1 << i%8)) >> i%8;
    buff_position += sel_length;
    return sel;

def load_float32(buff):
    global buff_position;
    result = np.frombuffer(buff, dtype = np.float32, count = 1, offset = buff_position);
    buff_position += 4;    
    return result[0];

def load_vector(buff, gene_count, long_add):
    global buff_position;
    result = np.zeros((gene_count,), dtype = np.float32);
    formatbyte = buff[buff_position];
    buff_position += 1;
    is_sparse = formatbyte & 1 > 0;
    is_byte_scaled = formatbyte & 2 > 0;
    if is_byte_scaled:
        min_v = load_float32(buff);
        max_v = load_float32(buff);
        
    if is_sparse:
        ncount = load_int(buff, long_add);
        
        for i in range(ncount):
            #loadvalue
            index = load_int(buff, long_add);    
            if is_byte_scaled:
                value = load_byte(buff);
                value = float(value)/255.0 * (max_v - min_v) + min_v;
            else:
                value = load_float32(buff);
            result[index] = value;
        
    else:
        for i in range(gene_count):
            #loadvalue
            if is_byte_scaled:
                value = load_byte(buff);
                value = float(value)/255.0 * (max_v - min_v) + min_v;
            else:
                value = load_float32(buff);
            result[i] = value;
        
    
    return result;


######################################################################################
#Initialization of the settings, paths and parameters, loading of data files
######################################################################################
settings_file = os.path.abspath(os.path.join(module_path, '../settings.ini'));
root_folder = os.path.dirname(settings_file);
global_settings = load_global_settings(settings_file);

logfile = os.path.join(global_settings['Default Paths']['Results_Path'], 'propagation.log');
start_log(logfile, autoclose = True, overwrite_existing = True, verbosity_level = 0);

######################################################################################
#Loading global data
######################################################################################

interactome_path = global_settings['Default Paths']['Interactome_Path'];
compounds_path = global_settings['Default Paths']['Compounds_Path'];
scratch_path = global_settings['Default Paths']['Scratch_Path'];

#Genes first
fname = os.path.join(interactome_path, 'genes.csv.gz');
printlog('Genes source file:');
printlog(fname);
printlog('Reading genes...');
genes = pd.read_csv(fname, index_col = 0);
gene_count = len(genes.index);
printlog('%s genes loaded.'%gene_count);

######################################################################################
#Checking if needed files from the previous step exist
######################################################################################

propagator = os.path.abspath(os.path.join(module_path, './Propagation/drugs_process'));
if not os.path.isfile(propagator):
    printlog('Error! %s not found! Please compile the C++ code first!'%propagator);
    stop_log();
    quit();

decoder = os.path.abspath(os.path.join(module_path, './Propagation/decode_from_base64'));    
if not os.path.isfile(decoder):
    printlog('Error! %s not found! Please compile the C++ code first!'%decoder);
    stop_log();
    quit();

task_file = os.path.abspath(os.path.join(global_settings['Default Paths']['Scratch_Path'], 'task.txt'))
if not os.path.isfile(task_file):
    printlog('Error! %s not found! Please run prepare_data.py first!'%task_file);
    stop_log();
    quit();

dataset_file = os.path.abspath(os.path.join(global_settings['Default Paths']['Scratch_Path'], 'base_dataset.txt'));
if not os.path.isfile(dataset_file):
    printlog('Error! %s not found! Please run prepare_data.py first!'%dataset_file);
    stop_log();
    quit();

result_file = os.path.abspath(os.path.join(global_settings['Default Paths']['Scratch_Path'], 'output.txt'));
decoded_result_file = os.path.abspath(os.path.join(global_settings['Default Paths']['Scratch_Path'], 'output.txt_0.dat'));

printlog('Starting propagation...');

try:
    subprocess.run([propagator, task_file, dataset_file, result_file], check = True)
except subprocess.CalledProcessError as err:
    printlog('Error running propagation: %s'%str(err))

printlog('Base64 decoding of propagated profiles...');

try:
    subprocess.run([decoder, result_file], check = True)
except subprocess.CalledProcessError as err:
    printlog('Error running decoding: %s'%str(err))
    
if not os.path.isfile(decoded_result_file):
    printlog('Error! %s not found! Propagation failed!'%decoded_result_file);
    stop_log();
    quit();

if global_settings.getboolean('Cleanup', 'cleanup_enabled'):
    printlog('Removing intermediate files...')    
    os.remove(result_file);
    os.remove(task_file);
    os.remove(dataset_file);

resvectors = [];
printlog('loading %s'%decoded_result_file);
with open(decoded_result_file, 'rb') as finp:
    buff = bytearray(finp.read());
        
buff_position = 9; 
hashbytes = np.array(buff[-32:], dtype = np.uint8);
buff = buff[:-32];
printlog('Calculating sha256 hash...');
hh = np.array(bytearray(hashlib.sha256(buff).digest()), dtype = np.uint8);
        
hashmatch = np.sum(hashbytes == hh) == 32;

if not hashmatch:
    printlog('Error! Returned file corrupted! Try re-running the propagation.');
    stop_log();
    quit();
            
format_byte = load_byte(buff);
long_address = format_byte & 1 > 0;
full_vectors = format_byte & 2 > 0;
cross_correlations = format_byte & 4 > 0;
all_combinations = format_byte & 8 > 0;
if (not all_combinations) and (not cross_correlations):
    full_vectors = True;
byte_return_format = format_byte & 16 > 0;
batch2_comblevel = int((format_byte & 0b11100000) >> 5) + 1;

assert(all_combinations == False);
assert(cross_correlations == False);
assert(full_vectors == True);

subset1_count = load_int(buff, False);
subset2_count = load_int(buff, False);

assert(subset2_count == 0)

uniqueID_A = load_int(buff, True);
uniqueID_B = load_int(buff, True);
task_subindex = load_int(buff, True);
                
n_combinations = load_int(buff, False);
count = subset1_count;

buff_position += 8;#time
                
for i in range(n_combinations):
    print('%s of %s'%(i, n_combinations));
    selection = load_selection(buff, count);
    if full_vectors:
        v1 = load_vector(buff, gene_count, long_address);
        resvectors.append(v1);
printlog('Size: %s'%len(resvectors))

resvectors = np.vstack(resvectors);
vectors_file = os.path.abspath(os.path.join(global_settings['Default Paths']['Scratch_Path'], 'vectors.dat'));
resvectors.tofile(vectors_file);

if global_settings.getboolean('Cleanup', 'cleanup_enabled'):
    printlog('Cleaning up...');
    os.remove(os.path.abspath(os.path.join(global_settings['Default Paths']['Scratch_Path'], 'base_dataset.meta')));
    os.remove(os.path.abspath(os.path.join(global_settings['Default Paths']['Scratch_Path'], 'task.meta')));
    os.remove(os.path.abspath(os.path.join(global_settings['Default Paths']['Scratch_Path'], 'output.txt_0.dat')));

printlog('Finished. Run machine learning section next!');

stop_log();