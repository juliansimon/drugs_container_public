/*  drugs_process.h

This is the header for drugs_process.cpp module. Include this h-file into your code and call 
process_datasets with file path arguments:
char* set_A - small size set with each line representing a few kB query (i.e. dataset A (profiles?/Analysis Tasks))
char* set_B - large (up to 5 Mb) set   (i.e. dataset B (Base Datasets?))
char* out_file - file name with file path to output results to. 

Any questions - please contact Dr. Ivan Laponogov, i.laponogov@imperial.ac.uk

Project DRUGS for Vodafone. 2017

Author: Dr. Ivan Laponogov
Supervisor: Dr. Kirill A. Veselkov

Copyright 2018 Imperial College London

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


*/

typedef unsigned char byte;
void init_decoder();
void decode_from_base64(byte* buffer, int buffer_size, byte** output_buffer, int* return_buffer_size);
void load_A_set(char* fname, byte*** result, int* buff_A_size, int** buff_A_sizes);
int process_datasets(char* set_A, char* set_B, char* out_file);
void verify_hash(byte *buff, int buff_size, byte *hashbuff);
byte load_byte(byte *buff, int *buff_position, int size);
int load_int(byte *buff, int *buff_position, int size, bool las);
int load_signed_int(byte *buff, int *buff_position, int size, bool las);
byte* get_sha256(byte *buff, long buff_size);
void encode_to_base64(byte* buffer, int buffer_size, byte** output_buffer, int* return_buffer_size);
float load_float(byte *buff, int *buff_position, int size);
void write_float(byte *buff, int *buff_position, float value);
double load_double(byte *buff, int *buff_position, int size);
void write_index(byte *buff, int *buff_position, int value, bool las);
void char_copy(byte *src, byte *des, int cnt);