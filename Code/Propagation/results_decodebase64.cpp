/*  results_decodebase64.cpp

This module is for fast task results extraction and base64 decoding

Any questions - please contact Dr. Ivan Laponogov, i.laponogov@imperial.ac.uk

Project DRUGS for Vodafone. 2017

Author: Dr. Ivan Laponogov
Supervisor: Dr. Kirill A. Veselkov


Copyright 2018 Imperial College London

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


*/
#include <iostream>
#include <fstream>
using namespace std;

#include <drugs_process.h>
#include <string>
#include <sstream>


namespace patch
{
    template < typename T > std::string to_string( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }
}

void load_A_set(char* fname, byte*** result, int* buff_A_size, int** buff_A_sizes) {

    streampos size;
    char * memblock;
    int memsize = 0;
    int i;
    int j;

    ifstream file (fname, ios::in|ios::binary|ios::ate);
    if (file.is_open()) {
        try{
            size = file.tellg();
            memblock = new char [size];
            memsize = size;
            file.seekg (0, ios::beg);
            file.read (memblock, size);
            file.close();


            #if cout_output_enabled    
            cout << "the entire file A content is in memory\n";
            cout << size << " bytes loaded\n";
            #endif
            bool after_newline = true;

            *buff_A_size = 0; 
            for (i = 0; i < size; ++i) {
                if ((int(*(memblock + i)) != 13) && (int(*(memblock + i)) != 10)) {
                    if (after_newline) {
                        *buff_A_size += 1;
                        after_newline = false;
                    };
                } else {
                    after_newline = true;
                };
            };
        
            *result = new byte* [*buff_A_size];
            *buff_A_sizes = new int [*buff_A_size];

            for (i = 0; i < *buff_A_size; ++i) {
                *(*buff_A_sizes + i) = 0;
            };
        
            int subcount = 0;
            int subindex = -1;

            after_newline = true;     
            bool in_line = false;

            for (i = 0; i < size; ++i) {
                if ((int(*(memblock + i)) != 13) && (int(*(memblock + i)) != 10)) {
                    in_line = true;
                    if (after_newline) {
                        subindex += 1;
                        subcount = 0;
                        after_newline = false;
                    };
                    subcount += 1;
                } else {
                    if (in_line) {
                        *((*buff_A_sizes) + subindex) = subcount;
                        (*(*result + subindex)) = new byte [subcount];
                        subcount = 0;
                    };
                    in_line = false;
                    after_newline = true;
                };
            };

            if (in_line) {
                *((*buff_A_sizes) + subindex) = subcount;
                (*(*result + subindex)) = new byte [subcount];
                subcount = 0;
            };
            
            #if cout_output_enabled
            cout << subindex + 1 << "non-empty lines found:\n";
            #endif
            //throw;
            subcount = 0;
            subindex = -1;

            after_newline = true;     
            in_line = false;


            for (i = 0; i < size; ++i) {
                if ((int(*(memblock + i)) != 13) && (int(*(memblock + i)) != 10)) {
                    in_line = true;
                    if (after_newline) {
                        subindex += 1;
                        subcount = 0;
                        after_newline = false;
                    };
                    *(*(*result + subindex) + subcount) = int(*(memblock + i));
                    //cout << *(*(*result + subindex) + subcount);
                    subcount += 1;
                } else {
                    if (in_line) {
                        #if cout_output_enabled
                        cout << "\n";
                        #endif
                        //*((*buff_A_sizes) + subindex) = subcount;
                        //(*(*result + subindex)) = new byte [subcount];
                      //  subcount = 0;
                    };
                    in_line = false;
                    after_newline = true;
                };
            };

            ///if (in_line) {
            //    *((*buff_A_sizes) + subindex) = subcount;
            //    (*(*result + subindex)) = new byte [subcount];
            //    subcount = 0;
            //};


            /*
            for (i = 0; i < size; ++i) {
                if (int(*(memblock + i)) == 13) {
                    subcount = 0;
                    subindex += 1;
                } else if (int(*(memblock + i)) != 10) {
                    *(*(*result + subindex) + subcount) = int(*(memblock + i));
                    subcount += 1;
                };
            };

            */



            if (memsize > 0) {
                delete [] memblock;
            }

            //throw;

        }
        catch (...) {
            #if cout_output_enabled    
            cout <<"Error reading file A...\n";
            #endif    
            if (memsize > 0) {
                delete [] memblock;
            }
            throw "Error reading A";
        };

    
    } else {
        #if cout_output_enabled    
        cout <<"Error! Could not open file with set A! \n";
        #endif
        throw "Could not open file A";
    };
}; 

int main (int argc, char** argv) {
    //Check it is compiled with 32bit int....
    init_decoder();
    int intsize = sizeof(int);
    if (intsize < 4) {
        cout << "\nError! Int is smaller than 4 bytes! Recompile with at least 32bit native architecture...\n";
        return -1;

    } 

    int floatsize = sizeof(float);
    if (floatsize != 4) {
        cout << "\nError! float size is not 4 bytes! ...\n";
        return -1;

    } 


    cout << "You have entered " << argc
         << " arguments:" << "\n";

    if (argc != 2) {
        cout <<"Not enough parameters! Supply results set!\n";
        return -1;
    };
    
    char *module_name = argv[0];
    char *set_A = argv[1];

    cout <<"Running module name: " << module_name << "\n";
    cout <<"Results A: " << set_A << "\n";

    byte **buff_A;
    int *buff_A_sizes;
    int buff_A_size = 0;

    //Load Results below and do one by one processing.......
    load_A_set(set_A, &buff_A, &buff_A_size, &buff_A_sizes);

    cout << "Loaded set A... \n";
    cout <<"Lines in set A to be processed: " << buff_A_size << "\n";

    int i;
    int j;

    byte *decoded_line_A;
    int line_A_size = 0;


    for (i = 0; i < buff_A_size; ++i) {
            cout <<"Processing line " << i + 1 << " of " << buff_A_size << "...\n";
            try {    
                decode_from_base64(*(buff_A + i), *(buff_A_sizes + i), &decoded_line_A, &line_A_size);
    
                string foutname(set_A);
    
                foutname += "_" + patch::to_string(i) + ".dat";
    
                cout << "Outfile: " << foutname << '\n';
    
                int j;
                ofstream outfile (foutname.c_str(), ios::out | ios::binary);
                outfile.write((char*)decoded_line_A, line_A_size);
                    
                outfile.close();
            } catch (...) {
                cout << "Skipping...\n";

            }


            if (line_A_size > 0) {
                delete [] decoded_line_A;
                line_A_size = 0;
            };
    };




    if (buff_A_size > 0)  {
    //Free set A memory
        for (i = 0; i < buff_A_size; ++i) {
            if (*(buff_A_sizes + i) > 0) {
                delete [] (*(buff_A + i));
            };
        };
        delete [] buff_A_sizes;
        delete [] buff_A;
    };


    cout <<"Finished\n";

    return 0;
}