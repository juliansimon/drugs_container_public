/*  drugs_process.cpp

This is the drugs_process.cpp module. Include corresponding h-file into your code and call 
process_datasets with file path arguments:

char* set_A - small size set with each line representing a few kB query
char* set_B - large (up to 5 Mb) set 
char* out_file - file name with file path to output results to. 

Any questions - please contact Dr. Ivan Laponogov, i.laponogov@imperial.ac.uk

Project DRUGS for Vodafone. 2017

Author: Dr. Ivan Laponogov
Supervisor: Dr. Kirill A. Veselkov


Copyright 2018 Imperial College London

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/



#include <iostream>
#include <string>
#include <math.h>
#include <fstream>
#include <stdlib.h>
#include <ctime>
#include <stdint.h>

#define cout_output_enabled true
#define global_error_throw_enabled true
#define global_time_recording_enabled false

using namespace std;
typedef unsigned char byte;

//Hash check

#define ROTR(x, n)  ((x >> n) | (x << (32 - n)))

byte* get_sha256(byte *buff, long buff_size) {
    byte *hash = new byte [32];

    uint32_t *hh = (uint32_t*) hash;
    //Initialize first hash
    hh[0] = 0x6a09e667;
    hh[1] = 0xbb67ae85;
    hh[2] = 0x3c6ef372;
    hh[3] = 0xa54ff53a;
    hh[4] = 0x510e527f;
    hh[5] = 0x9b05688c;
    hh[6] = 0x1f83d9ab;
    hh[7] = 0x5be0cd19;

    const uint32_t k[64] = {
       0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
       0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
       0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
       0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
       0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
       0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
       0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
       0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2};
    
    long l = buff_size * 8 + 65;
    int frac = l % 512; 
    if (frac != 0) {
        l = l + 512 - frac;  
    };

    l = l / 32;
    
    uint32_t *m = new uint32_t [l];
    long i;
    
    byte *m_cast = (byte*) m;

    for (i = 0; i < buff_size; i++) {
        m_cast[i] = buff[i];
    };

    m_cast[buff_size] = 128;
    for (i = buff_size + 1; i < l * 4 - 8; i++) {
        m_cast[i] = 0;
    };

    m_cast[l * 4 - 1] = ((buff_size << 3) >>  0) & 0xff;
    m_cast[l * 4 - 2] = ((buff_size << 3) >>  8) & 0xff;
    m_cast[l * 4 - 3] = ((buff_size << 3) >> 16) & 0xff;
    m_cast[l * 4 - 4] = ((buff_size << 3) >> 24) & 0xff;
    m_cast[l * 4 - 5] = ((buff_size << 3) >> 32) & 0xff;
    m_cast[l * 4 - 6] = ((buff_size << 3) >> 40) & 0xff;
    m_cast[l * 4 - 7] = ((buff_size << 3) >> 48) & 0xff;
    m_cast[l * 4 - 8] = ((buff_size << 3) >> 56) & 0xff;

    for (i = 0; i < l; i++) {
        uint32_t tmp = m[i];
        m_cast[i * 4 + 0] = (tmp >> 24) & 0xff;
        m_cast[i * 4 + 1] = (tmp >> 16) & 0xff;
        m_cast[i * 4 + 2] = (tmp >> 8) & 0xff;
        m_cast[i * 4 + 3] = (tmp >> 0) & 0xff;
    };

    for (long chunk = 0; chunk < l / 16; chunk++) {
        uint32_t w[64];
        for (i = 0; i < 16; i++) {
            w[i] = m[chunk * 16 + i];
        };

        for (i = 16; i < 64; i++) {
            uint32_t s0 = (ROTR(w[i - 15], 7) ^ ROTR(w[i - 15], 18) ^ (w[i - 15] >> 3));
            uint32_t s1 = (ROTR(w[i - 2], 17) ^ ROTR(w[i - 2], 19) ^ (w[i - 2] >> 10));
            w[i] = w[i - 16] + s0 + w[i - 7] + s1;
        };

        uint32_t a = hh[0];
        uint32_t b = hh[1];
        uint32_t c = hh[2];
        uint32_t d = hh[3];
        uint32_t e = hh[4];
        uint32_t f = hh[5];
        uint32_t g = hh[6];
        uint32_t h = hh[7];

        for (i = 0; i < 64; i++) {
            uint32_t s1 = (ROTR(e, 6) ^ ROTR(e, 11) ^ ROTR(e, 25));
            uint32_t ch = (e & f) ^ ((~e) & g);
            uint32_t temp1 = h + s1 + ch + k[i] + w[i];
            uint32_t s0 = ROTR(a, 2) ^ ROTR(a, 13) ^ ROTR(a, 22);
            uint32_t maj = (a & b) ^ (a & c) ^ (b & c);
            uint32_t temp2 = s0 + maj;
            h = g;
            g = f;
            f = e;
            e = d + temp1;
            d = c;
            c = b;
            b = a;
            a = temp1 + temp2;
        };

        hh[0] += a;
        hh[1] += b;
        hh[2] += c;
        hh[3] += d;
        hh[4] += e;
        hh[5] += f;
        hh[6] += g;
        hh[7] += h;
    };
    
    
    for (i = 0; i < 8; i++) {
        uint32_t tmp = hh[i];
        hash[i * 4 + 0] = (tmp >> 24) & 0xff;
        hash[i * 4 + 1] = (tmp >> 16) & 0xff;
        hash[i * 4 + 2] = (tmp >> 8) & 0xff;
        hash[i * 4 + 3] = (tmp >> 0) & 0xff;
    };
    

    delete [] m;  
    
    return hash;
};



void verify_hash(byte *buff, int buff_size, byte *hashbuff) {
#if cout_output_enabled
    cout << "Checking hash\n";
#endif
    int i;
    byte *sha;
    sha = get_sha256(buff, buff_size);
    bool matched = true;
    for (i = 0; i < 32; ++i) {
/*        #if cout_output_enabled
        cout << int(*(sha + i)) << "==" << int(*(hashbuff + i)) << "\n";
        #endif
*/
        if (*(sha + i) != *(hashbuff + i)) {
            matched = false;
            break;
        };
    };        
    delete [] sha; 

    if (!matched) {
        #if cout_output_enabled
        cout <<"Hash mismatch!\n";
        #endif
        throw "Hash mismatch!";
    };
};



//Basic mean of array of floats

static float abs_value (float x) {
    if (x>0.0) {
        return x;
    } else {
        return -x;
    };
};

static double mean(float *x, float *w, int size) {
    int i;
    double sum = 0.0;       
    double sumw = 0.0;

    for (i = 0; i < size; ++i) {
        sum += (*(x + i)) * (*(w + i));
        sumw += *(w + i);
    };
   
    return sum / sumw;
}

void write_index(byte *buff, int *buff_position, int value, bool las) {
    if (las) {
        *(buff + *buff_position) = value & 0b11111111;
        *(buff + *buff_position + 1) = (value >> 8) & 0b11111111;
        *(buff + *buff_position + 2) = (value >> 16) & 0b11111111; 
        *(buff + *buff_position + 3) = (value >> 24) & 0b11111111;
        *buff_position += 4;
    } else {
        *(buff + *buff_position) = value & 0b11111111;
        *(buff + *buff_position + 1) = (value >> 8) & 0b11111111;
        *buff_position += 2;
    };
};

int load_int(byte *buff, int *buff_position, int size, bool las) {
            int result;
            if (las) {
                result = int(*(buff + *buff_position)) |
                         int(*(buff + *buff_position + 1)) << 8 |   
                         int(*(buff + *buff_position + 2)) << 16 |                           
                         int(*(buff + *buff_position + 3)) << 24;                           
                *buff_position += 4;
            } else {

                result = int(*(buff + *buff_position)) |
                         int(*(buff + *buff_position + 1)) << 8;                           
                *buff_position += 2;
            };
            return result;
 };

float load_float(byte *buff, int *buff_position, int size) {
            void *ptr;
            ptr = (buff + *buff_position);
            float *fptr = (float*) ptr;
            *buff_position += 4;
            return *fptr;
 };


void write_float(byte *buff, int *buff_position, float value) {
            void *ptr;
            ptr = (buff + *buff_position);
            float *fptr = (float*) ptr;
            *fptr = value;
            *buff_position += 4;
 };

static void write_double(byte *buff, int *buff_position, double value) {
            void *ptr;
            ptr = (buff + *buff_position);
            double *fptr = (double*) ptr;
            *fptr = value;
            *buff_position += 8;
 };

double load_double(byte *buff, int *buff_position, int size) {
            void *ptr;
            ptr = (buff + *buff_position);
            double *fptr = (double*) ptr;
            *buff_position += 8;
            return(*fptr);
 };


byte load_byte(byte *buff, int *buff_position, int size) {
            byte result = *(buff + *buff_position);
            *buff_position += 1;
            return result;
 };

static void write_byte(byte *buff, int *buff_position, byte value) {
            *(buff + *buff_position) = value;
            *buff_position += 1;
 };


static int load_signed_byte(byte *buff, int *buff_position, int size) {
            byte result = *(buff + *buff_position);
            *buff_position += 1;
            bool negative = (result & 0b10000000) > 0;
            int v = result & 0b01111111;
            if (negative) {
                v = v - 128;
            }
            return v;
 };

int load_signed_int(byte *buff, int *buff_position, int size, bool las) {
            int result;
            if (las) {
                void *ptr;
                ptr = (buff + *buff_position);
                int *iptr = (int*) ptr;
                *buff_position += 4;
                return *iptr;    
            } else {

                result = int(*(buff + *buff_position)) |
                         int(*(buff + *buff_position + 1)) << 8;                           
                *buff_position += 2;

                if ((result & 0b1000000000000000) > 0) {
                    result = (result & 0b0111111111111111) - 0b1000000000000000;
                };

                return result;

            };
 };




static bool* decode_bit_compressed(byte *buff, int *buff_position, int mut_count) {
            int f = mut_count / 8;
            int rem = mut_count % 8;
            int tot_count = f;
            int i;
            if (rem > 0) {
                tot_count += 1;    
            };
            bool *result;
            result = new bool [tot_count * 8];
            
            byte v;

            for (i = 0; i < tot_count; ++i) {
               v =  load_byte(buff, buff_position, 0);
               *(result + (i*8) + 0) = ((v & 1) > 0);
               *(result + (i*8) + 1) = ((v & 2) > 0);
               *(result + (i*8) + 2) = ((v & 4) > 0);
               *(result + (i*8) + 3) = ((v & 8) > 0);
               *(result + (i*8) + 4) = ((v & 16) > 0);
               *(result + (i*8) + 5) = ((v & 32) > 0);
               *(result + (i*8) + 6) = ((v & 64) > 0);
               *(result + (i*8) + 7) = ((v & 128) > 0);
            };    

            return result;
 };



static void double_copy(double *src, double *des, int cnt) {
            int i;
            for (i = 0; i < cnt; ++i) {
                *(des + i) = *(src + i);
            };    
 };


void char_copy(char *src, char *des, int cnt) {
    int i;
    for (i = 0; i < cnt; ++i) {
        *(des + i) = *(src + i);
    };    
 };


static void float_copy(float *src, float *des, int cnt) {
            int i;
            for (i = 0; i < cnt; ++i) {
                *(des + i) = *(src + i);
            };    
 };


static void bool_copy(bool *src, bool *des, int cnt) {
            int i;
            for (i = 0; i < cnt; ++i) {
                *(des + i) = *(src + i);
            };    
 };


static double get_sum(double *src, int cnt) {
            double sum = 0.0;
            int i;
            for (i = 0; i < cnt; ++i) {
                sum += *(src + i);
            };    
            return sum;
 };


static void zero_it(double *src, int cnt) {
            int i;
            for (i = 0; i < cnt; ++i) {
                *(src + i) = 0.0;
            };    
 };

static void set_bool(bool *src, int cnt, bool val) {
            int i;
            for (i = 0; i < cnt; ++i) {
                *(src + i) = val;
            };    
 };


static double get_max_diff(double *src, double *des, int cnt) {
            int i;
            double maxd;
            double diff;
            int indx;
            double v1;
            double v2;
            maxd = 0.0;
            //v1 = 0.0;
            //v2 = 0.0;
            //indx = 0;
            for (i = 0; i < cnt; ++i) {
                diff = *(des + i) - *(src + i);
                if (diff < 0.0) {
                    diff = -diff;
                };
                if (diff > maxd) {
                    maxd = diff;
                    //indx = i;
                    //v1 = *(des + i);
                    //v2 = *(src + i);
                };
            };    
            //cout << "indx " << indx <<" v1 " <<v1 << " v2 " << v2 << "\n";
            return maxd;
 };


//Correlation
static double correlation(float *x, float *y, float *w, int size) {

    double mx = mean(x, w, size);
    double my = mean(y, w, size);

    int i;

    double cov = 0.0;
    double cx = 0.0;
    double cy = 0.0;
    double dx = 0.0;
    double dy = 0.0;

    for (i = 0; i < size; ++i) {
        dx = *(x + i) - mx;
        dy = *(y + i) - my;
        cov += dx * dy * (*(w + i));
        cx  += dx * dx * (*(w + i));
        cy  += dy * dy * (*(w + i));
    }

    return cov/(sqrt(cx*cy));
    
}


struct Node {
    int node_index;
    float edge;
};

struct RootNode {
    int count;
    struct Node *connected_nodes;
};

struct Mutation {
    int position;
    float value;
};

struct Sample {
    int count;
    struct Mutation *mutations;
};


class Interactome {
    public:
        int count;
        struct RootNode *root_nodes;
        struct RootNode *root_nodes1;
        struct RootNode *root_nodes2;
        float *genes_default;
        float *genes;
        int method;        // 'PageRank':0, 'SimpleFlow':1,'SteadyFlow':2,
        int method1;
        int method2;
        
        bool matrix_normalize;
        bool matrix_normalize_incoming;
        bool matrix_normalize_to_sum;

        float matrix_scale;
        bool large_address_size; //Use 16 bit  or 32 bit
        //bool byte_compressed_output;
        int unique_ID;

        int iterations_limit;
        float eps;
        int init_ilimit;
        float init_eps;
        float zero_eps;
        //PageRank
        float c_const;
        bool normalize;
        bool post_normalize;
        //SimpleFlow exclusive
        float damping;
        bool use_scaler;
        bool use_max;
        //SteadyFlow exclusive
        bool use_difference;
        bool use_relative_change;
        bool use_log_change;
        float dt;
        //RubberNet
        float power_value;

        float *sink_default;
        float *source_default;
        bool use_custom_sink_source;




        int iterations_limit1;
        float eps1;
        int init_ilimit1;
        float init_eps1;
        float zero_eps1;
        //PageRank
        float c_const1;
        bool normalize1;
        bool post_normalize1;
        //SimpleFlow exclusive
        float damping1;
        bool use_scaler1;
        bool use_max1;
        //SteadyFlow exclusive
        bool use_difference1;
        bool use_relative_change1;
        bool use_log_change1;
        float dt1;
        //RubberNet
        float power_value1;

        float *sink_default1;
        float *source_default1;
        float *genes_default1;


        bool use_custom_sink_source1;


        int iterations_limit2;
        float eps2;
        int init_ilimit2;
        float init_eps2;
        float zero_eps2;
        //PageRank
        float c_const2;
        bool normalize2;
        bool post_normalize2;
        //SimpleFlow exclusive
        float damping2;
        bool use_scaler2;
        bool use_max2;
        //SteadyFlow exclusive
        bool use_difference2;
        bool use_relative_change2;
        bool use_log_change2;
        float dt2;
        //RubberNet
        float power_value2;

        float *sink_default2;
        float *source_default2;
        float *genes_default2;
        bool use_custom_sink_source2;

        float *sink;
        float *source;
        float *weights;

        //set which subset of settings to use
        void set_settings(int settings_set) {
            if (settings_set == 1) {
                method = method1;
                root_nodes = root_nodes1;
                iterations_limit = iterations_limit1;
                eps = eps1;
                init_ilimit = init_ilimit1;
                init_eps = init_eps1;
                zero_eps = zero_eps1;
                c_const = c_const1;
                normalize = normalize1;
                post_normalize = post_normalize1;
                damping = damping1;
                use_scaler = use_scaler1;
                use_max = use_max1;
                use_difference = use_difference1;
                use_relative_change = use_relative_change1;
                use_log_change = use_log_change1;
                dt = dt1;
                power_value = power_value1;
                sink_default = sink_default1;
                source_default = source_default1;
                genes_default = genes_default1;
                use_custom_sink_source = use_custom_sink_source1;
            } else {
                method = method2;
                root_nodes = root_nodes2;
                iterations_limit = iterations_limit2;
                eps = eps2;
                init_ilimit = init_ilimit2;
                init_eps = init_eps2;
                zero_eps = zero_eps2;
                c_const = c_const2;
                normalize = normalize2;
                post_normalize = post_normalize2;
                damping = damping2;
                use_scaler = use_scaler2;
                use_max = use_max2;
                use_difference = use_difference2;
                use_relative_change = use_relative_change2;
                use_log_change = use_log_change2;
                dt = dt2;
                power_value = power_value2;
                sink_default = sink_default2;
                source_default = source_default2;
                genes_default = genes_default2;
                use_custom_sink_source = use_custom_sink_source2;
            };
        };

        //set initial state of the model to default
        void initialize_from_default() {
            int i;
            for (i = 0; i < count; ++i) {
                *(genes + i) = *(genes_default + i);
                *(sink + i) = *(sink_default + i);
                *(source + i) = *(source_default + i);
            };
        };

        void print_curr_stats(double *current) {
            int i;
            double min;
            double max;
            double mean;
            min = 1e20;
            max = 1e-20;
            mean = 0.0;
            for (i = 0; i < count; ++i) {
                if (*(current + i) > max) {
                    max = *(current + i);
                };
                if (*(current + i) < min) {
                    min = *(current + i);
                };
                mean += *(current + i);
            };
            mean /= count;
            #if cout_output_enabled
            cout << "Min gene value: " << min << "\n";
            cout << "Max gene value: " << max << "\n";
            cout << "Mean gene value: " << mean << "\n";
            #endif
        };

        //Random walk propagation algorithm
        void propagate_RandomWalk() {
            #if cout_output_enabled
            cout << "Propagation via RandomWalk...\n";
            #endif
            double *p_start;
            double *p_prev;
            //double *p_osc_prev;
            double *p_current;
            double eval_eps;
            double eval_eps2;
            int i;
            int j;
            double sum;
            int iter_count;
            p_start = new double [count];
            p_prev = new double [count];
            p_current = new double [count];

            try {
                #if cout_output_enabled
                    cout << "c_const for PageRank: " <<c_const << "\n";
                #endif

                //Load initial values for gene perturbation to p_start
                for (i = 0; i < count; ++i) {
                    *(p_start + i) = *(genes + i);
                };
    
                //Normalize input
                sum = get_sum(p_start, count);
                if (sum != 0.0) {
                    for (i = 0; i < count; ++i) {
                        *(p_start + i) /= sum;
                    };
                };
                
                // Set previous gene levels to 0                
                zero_it(p_prev, count);
                //zero_it(p_osc_prev, count);
                
                // Copy starting normalized gene values to current set
                double_copy(p_start, p_current, count);
                //Get initial max difference between previous and current sets of gene values
                eval_eps = get_max_diff(p_prev, p_current, count);
                iter_count = 0; //Reset iterations counter
                eval_eps2 = eval_eps;
                print_curr_stats(p_current);
                //print_curr_stats(p_prev);
                double v;
                struct RootNode *tempNode;
                int cnt;
                //Until eps level is satisfied or iterations are exceeding limit iterate:
                while ((eval_eps > eps) && (iter_count < iterations_limit)) {
                    //copy current to previous state
                    //double_copy(p_prev, p_osc_prev, count);
                    double_copy(p_current, p_prev, count);
                    // zero current state 
                    zero_it(p_current, count);
    
                    for (i = 0; i < count; ++i) { //Cycle over all nodes
                       tempNode = (root_nodes + i);
                       v = *(p_prev + i); //Probability of the source node
                       cnt = (*tempNode).count; //Count of connected nodes
                       for (j = 0; j < cnt; ++j) {
                            //Propagate probability to connected nodes as edge weight * probability of the current node.
                            *(p_current + (*((*tempNode).connected_nodes + j)).node_index) += 
                                          (*((*tempNode).connected_nodes + j)).edge * v; 
                        };
    
                    };
    
                   for (i = 0; i < count; ++i) { //Cycle over all nodes
                        *(p_current + i) = *(p_current + i) * (1.0 - c_const) + c_const * *(p_start + i);
                   };
    
                    eval_eps = get_max_diff(p_prev, p_current, count);
                    
                    #if cout_output_enabled
                    cout << "eps " <<eval_eps << "\n";
                    #endif
                    iter_count += 1;
                };
                #if cout_output_enabled    
                cout << "Iterations passed.." << iter_count << "\n";
                #endif
                print_curr_stats(p_current);
    
                //Apply post-normalization if needed            
                if (post_normalize) {
                    sum = get_sum(p_current, count);
                    if (sum != 0.0) {
                        for (i = 0; i < count; ++i) {
                            *(p_current + i) /= sum;
                        };
                    };
                };
                
                //Write results back
                for (i = 0; i < count; ++i) {
                    *(genes + i) = *(p_current + i);
                }
            }


            catch (...) {
                #if cout_output_enabled
                cout <<"Error in Random Walk!\n";
                #endif
                delete [] p_current;
                delete [] p_prev;
                delete [] p_start;    
                throw "Error in Random Walk!";
            };


            //Clean up
            delete [] p_current;
            delete [] p_prev;
            delete [] p_start;    
            //delete [] p_osc_prev;

        };

        //Simple unidirectional flow propagation algorithm
        void propagate_SimpleFlow() {
            #if cout_output_enabled    
            cout << "Propagation via SimpleFlow...\n";
            #endif
            double *p_prev;
            double *p_current;
            double eval_eps;
            int i;
            int j;
            int iter_count;
            p_prev = new double [count];
            p_current = new double [count];
            double *scaler;

            if (use_scaler) {
                scaler = new double [count];
            };

            try {
                //Initialize current state
                for (i = 0; i < count; ++i) {
                    *(p_current + i) = *(genes + i);
                };
    
                //Set previous state to zero
                zero_it(p_prev, count);
                
                //Get first max difference and reset iterations counter
                eval_eps = get_max_diff(p_prev, p_current, count);
                iter_count = 0;
    
                struct RootNode *tempNode;
                int cnt;
                double dv;
    
                if (use_scaler) {
                    zero_it(scaler, count);
                    for (i = 0; i < count; ++i) {
                            tempNode = (root_nodes + i);
                            cnt = (*tempNode).count; //Count of connected nodes
                            //propagate connection count
                            for (j = 0; j < cnt; ++j) {
                                *(scaler + (*((*tempNode).connected_nodes + j)).node_index) += 1.0;
                            };
                    };
                };
    
                //Iterate till difference is less or equal eps or iterations limit is not exceeded.
                while ((eval_eps > eps) && (iter_count < iterations_limit)) {
                    //copy current set to previous
                    double_copy(p_current, p_prev, count);
                    //Iterate over nodes
                    for (i = 0; i < count; ++i) {
                            tempNode = (root_nodes + i);
                            cnt = (*tempNode).count; //Count of connected nodes
                            //propagate influence
                            for (j = 0; j < cnt; ++j) {
                                if (*(p_prev + (*((*tempNode).connected_nodes + j)).node_index) == 0.0) {
    
                                    dv = (*((*tempNode).connected_nodes + j)).edge * *(p_prev + i) * damping; 
                                    if (use_max) {
                                        if (dv > *(p_current + (*((*tempNode).connected_nodes + j)).node_index)) {
                                            *(p_current + (*((*tempNode).connected_nodes + j)).node_index) = dv;
                                        };
    
                                    } else {
                                        *(p_current + (*((*tempNode).connected_nodes + j)).node_index) += dv;
                                    };          
                                };  
                            };
                    };
                    if (use_scaler) {
                        for (i = 0; i < count; ++i) {
                            if (*(p_prev + i) == 0.0) {
                                if (*(scaler + i) != 0.0) {
                                    *(p_current + i) /= *(scaler + i);
                                };
                            };
                        };    
                    };   
    
                    eval_eps = get_max_diff(p_prev, p_current, count); //Re-calculate max difference
                    print_curr_stats(p_current);
                    #if cout_output_enabled
                    cout << "eps " <<eval_eps << "\n";
                    #endif
                    iter_count += 1; //Increase iteration count
                };
                #if cout_output_enabled    
                cout << "Iterations passed.." << iter_count << "\n";
                #endif
                //Write results back
                for (i = 0; i < count; ++i) {
                    *(genes + i) = *(p_current + i);
                };


            }

            catch (...) {
                #if cout_output_enabled
                cout <<"Error in SimpleFlow...\n";
                #endif
                if (use_scaler) {
                    delete [] scaler;
                };
    
                delete [] p_current;
                delete [] p_prev;

                throw "Error in SimpleFlow";
            };

            //Clean up
            if (use_scaler) {
                delete [] scaler;
            };

            delete [] p_current;
            delete [] p_prev;

        };


        //RubberNet propagation algorithm
        void propagate_RubberNet() {
            #if cout_output_enabled
            cout << "Propagation via RubberNet...\n";
            #endif
            double *p_prev;
            double *p_current;
            double eval_eps;
            int i;
            int j;
            int iter_count;
            struct RootNode *tempNode;
            int cnt;
            double *scaler;
            p_prev = new double [count];
            p_current = new double [count];
            if (use_scaler) {
                scaler = new double [count];
            };

            try {
                //Initialize current state
                for (i = 0; i < count; ++i) {
                    *(p_current + i) = *(genes + i);
                };
    
                //Set previous state to zero
                zero_it(p_prev, count);
                
                //Get first max difference and reset iterations counter
                eval_eps = get_max_diff(p_prev, p_current, count);
                iter_count = 0;
    
                #if cout_output_enabled
                cout << "use scaler "<< use_scaler <<"\n";
                #endif
                if (use_scaler) {
                    zero_it(scaler, count);
                    for (i = 0; i < count; ++i) {
                            tempNode = (root_nodes + i);
                            cnt = (*tempNode).count; //Count of connected nodes
                            //propagate connection count
                            for (j = 0; j < cnt; ++j) {
                                *(scaler + (*((*tempNode).connected_nodes + j)).node_index) += 1.0;
                            };
                    };
                    #if cout_output_enabled
                    cout << "scaler " << *scaler <<"\n";
                    #endif
                };
                #if cout_output_enabled
                cout << "power_value " << power_value <<"\n";
                #endif
                //Iterate till difference is less or equal eps or iterations limit is not exceeded.
                while ((eval_eps > eps) && (iter_count < iterations_limit)) {
                    //copy current set to previous
                    double_copy(p_current, p_prev, count);
                    //Iterate over nodes
                    //zero current state to use for difference accumulation
                    zero_it(p_current, count);
    
                    //accumulate unfixed influence
                    for (i = 0; i < count; ++i) {
                            tempNode = (root_nodes + i);
                            cnt = (*tempNode).count; //Count of connected nodes
                            //propagate influence
                            for (j = 0; j < cnt; ++j) {
                                if (*(genes + (*((*tempNode).connected_nodes + j)).node_index) == 0.0) {
                                    *(p_current + (*((*tempNode).connected_nodes + j)).node_index) += 
                                        (*((*tempNode).connected_nodes + j)).edge * *(p_prev + i); 
                                    
                                };  
                            };
                    };
    
                    if (use_scaler) {
                        for (i = 0; i < count; ++i) {
                            if (*(genes + i) == 0.0) {
                                *(p_current + i) /= *(scaler + i);
                            };
                        };    
                    };   
    
                    for (i = 0; i < count; ++i) {
                        if (*(genes + i) == 0.0) {
                            if (*(p_current + i) >= 0) {
                                *(p_current + i) = 1 - pow(exp(-(*(p_current + i))), power_value);
                            } else {
                                *(p_current + i) = -1 + pow(exp(*(p_current + i)), power_value);
                            };
                        } else {
                            *(p_current + i) = *(genes + i);
                        };
                    }                    
    
                    eval_eps = get_max_diff(p_prev, p_current, count); //Re-calculate max difference
                    print_curr_stats(p_current);           
                    #if cout_output_enabled
                    cout << "eps " <<eval_eps << "\n";
                    #endif
                    iter_count += 1; //Increase iteration count
                };
                #if cout_output_enabled
                cout << "Iterations passed.." << iter_count << "\n";
                #endif
                //Write results back
                for (i = 0; i < count; ++i) {
                    *(genes + i) = *(p_current + i);
                }

            }

            catch (...) {
                #if cout_output_enabled
                cout <<"Error in RubberNet....\n";
                #endif
                if (use_scaler) {
                    delete [] scaler;
                };
    
                delete [] p_current;
                delete [] p_prev;
                throw "Error in RubberNet";

            };
            //Clean up
            if (use_scaler) {
                delete [] scaler;
            };

            delete [] p_current;
            delete [] p_prev;

        };


        void get_derivatives(double *k, double *p, int cnt) {
            struct RootNode *tempNode;
            int ncnt;
            int i;
            int j;

            zero_it(k, cnt);

            for (i = 0; i < count; ++i) { 
                tempNode = (root_nodes + i);
                ncnt = (*tempNode).count; //Count of connected nodes
                //Go through connections
                for (j = 0; j < ncnt; ++j) {
                    *(k + (*((*tempNode).connected_nodes + j)).node_index) += 
                          (*((*tempNode).connected_nodes + j)).edge * *(p + i);
                    *(k + i) -= (*((*tempNode).connected_nodes + j)).edge * *(p + i);                      
                };
                *(k + i) += *(source + i);  // Adding source
                *(k + i) -= *(sink + i) * *(p + i);  // Subtracting sink
            
            }
        };

        //SteadyFlow propagation algorithm        
        void propagate_SteadyFlow(float eps_v, int iterations_limit_v) {
            #if cout_output_enabled
            cout << "Propagation via SteadyFlow...\n";
            #endif
            double *p_prev;
            double *p_current;
            double eval_eps;
            int i;
            int j;
            int iter_count;
            p_prev = new double [count];
            p_current = new double [count];

            double *k1;
            double *k2;
            double *k3;
            double *k4;    
            k1 = new double [count];
            k2 = new double [count];
            k3 = new double [count];
            k4 = new double [count];

            try {

    
                //Initialize current state
                for (i = 0; i < count; ++i) {
                    *(p_current + i) = *(genes + i);
                };
    
                //Set previous state to zero
                zero_it(p_prev, count);
                
                //Get first max difference and reset iterations counter
                eval_eps = get_max_diff(p_prev, p_current, count);
                iter_count = 0;
    
    
                //Iterate till difference is less or equal eps or iterations limit is not exceeded.
                while ((eval_eps > eps_v) && (iter_count < iterations_limit_v)) {
                    //copy current set to previous
                    double_copy(p_current, p_prev, count);
                    zero_it(p_current, count);
                    
                    get_derivatives(k1, p_prev, count);
    
                    //y for k2                
                    for (i = 0; i < count; ++i) { 
                        *(p_current + i) = *(p_prev + i) + (dt * (*(k1 + i) / 2.0));
                    };
                    get_derivatives(k2, p_current, count);
                  
                    //y for k3                
                    for (i = 0; i < count; ++i) { 
                        *(p_current + i) = *(p_prev + i) + (dt * (*(k2 + i) / 2.0));
                    };
                    get_derivatives(k3, p_current, count);
    
                    //y for k4                
                    for (i = 0; i < count; ++i) { 
                        *(p_current + i) = *(p_prev + i) + (dt * *(k2 + i));
                    };
                    get_derivatives(k4, p_current, count);
    
                    //Runge-Kutta basic summation step
                    for (i = 0; i < count; ++i) { 
                        *(p_current + i) = *(p_prev + i) + (dt/6.0) * (*(k1 + i) + 2 * *(k2 + i) + 2 * *(k3 + i) + *(k4 + i));
                    };
    
                    eval_eps = get_max_diff(p_prev, p_current, count); //Re-calculate max difference
                    //cout <<"eps " <<eval_eps <<"\n";
                    //cout <<"p "<<*p_current <<"\n";
                    print_curr_stats(p_current);
                    iter_count += 1; //Increase iteration count
                    #if cout_output_enabled
                    cout << "eps " <<eval_eps << "\n";
                    #endif
                    
                };
                #if cout_output_enabled
                cout << "\nIterations passed.." << iter_count << "\n";
                cout << "Eps: " << eval_eps << "\n";
                #endif
                //Write results back
                for (i = 0; i < count; ++i) {
                    *(genes + i) = *(p_current + i);
                }
            }
            catch (...) {
                #if cout_output_enabled
                cout << "Error in SteadyFlow... \n";
                #endif
                delete [] k1;
                delete [] k2;
                delete [] k3;
                delete [] k4;
                delete [] p_current;
                delete [] p_prev;
                throw "Error in SteadyFlow";
            };
            //Clean up
            delete [] k1;
            delete [] k2;
            delete [] k3;
            delete [] k4;
            delete [] p_current;
            delete [] p_prev;
        };


        //propagate gene disturbance through the interactome according to the method selected
        void propagate(struct Mutation *vector, int vector_length) {
            int i;
            int j;
            float mvalue;
            initialize_from_default();

            if (method == 0 || method == 1 || method == 3) { //Simply add mutation values to corresponding genes for these methods
                for (i = 0; i < vector_length; ++i) {
                    *(genes + (*(vector + i)).position) = (*(vector + i)).value;
                };
            } else if (method == 2) {
                for (i = 0; i < vector_length; ++i) {
                    mvalue = (*(vector + i)).value;
                    if (mvalue >= 0.0) {
                        *(source + (*(vector + i)).position) = mvalue;
                    } else {
                        *(sink + (*(vector + i)).position) = -mvalue;
                    };
                };
            };

            if (method == 0) {
                propagate_RandomWalk();
            } else if (method == 1) {
                propagate_SimpleFlow();
            } else if (method == 2) {
                propagate_SteadyFlow(eps, iterations_limit);
            } else if (method == 3) {
                propagate_RubberNet();
            };
            
            //Post-processing for method SteadyFlow
            if (method == 2) {
                 if (use_difference){
                    for (i = 0; i < count; ++i) {
                        *(genes + i) -= *(genes_default + i);
                    }
                 };

                 if (use_relative_change){
                    for (i = 0; i < count; ++i) {
                        if (abs_value(*(genes_default + i)) > 1e-12) {
                            *(genes + i) /= *(genes_default + i);
                        } else {
                            *(genes + i) /= 1e-12;
                        };
                    }
                 };

                if (use_log_change){
                    for (i = 0; i < count; ++i) {
                        if (*(genes + i) >= 0.0) {
                            *(genes + i) = log(*(genes + i) + 1.0);
                        } else {
                            *(genes + i) = log(-(*(genes + i)) + 1.0);
                        };
                    }
                };
            };
        };



        void normalize_matrix() {
            int i;
            int j;
            double sum;
            double min;
            double max;
            double mean;
            int sum_i;
            int min_i;
            int max_i;
            float mean_i;
            int cnt;
            double edge_scale;            
            double *edge_scales;
            struct Node *tempNode;
            struct RootNode *tempRootNode;
            int conn_count;

            if (matrix_normalize) {
                if (matrix_normalize_incoming) {
                    edge_scales = new double [count];
                    for (i = 0; i < count; ++i) {
                        *(edge_scales + i) = 0.0;
                    };

                    for (i = 0; i < count; ++i) {
                        tempRootNode = (root_nodes + i);                    
                        conn_count = (*tempRootNode).count;
                        if (conn_count > 0) {
                            for (j = 0; j < conn_count; ++j) {
                                if (matrix_normalize_to_sum) {
                                    *(edge_scales + (*((*tempRootNode).connected_nodes + j)).node_index) += (*((*tempRootNode).connected_nodes + j)).edge;
                                } else {
                                    *(edge_scales + (*((*tempRootNode).connected_nodes + j)).node_index) += 1.0;
                                };
                            };
                            
                            for (j = 0; j < conn_count; ++j) {
                                if (*(edge_scales + (*((*tempRootNode).connected_nodes + j)).node_index) != 0.0) {
                                    (*((*tempRootNode).connected_nodes + j)).edge /= *(edge_scales + (*((*tempRootNode).connected_nodes + j)).node_index);
                                };
                            };
                        };    
                    };

                    delete [] edge_scales;
                } else {  //normalize outgoing connections
                    for (i = 0; i < count; ++i) {
                        tempRootNode = (root_nodes + i);                    
                        conn_count = (*tempRootNode).count;
                        if (conn_count > 0) {
                            if (matrix_normalize_to_sum) {
                                edge_scale = 0.0;
                                for (j = 0; j < conn_count; ++j) {
                                    edge_scale += (*((*tempRootNode).connected_nodes + j)).edge;
                                };
                            } else {
                                edge_scale = conn_count;
                            };
                            for (j = 0; j < conn_count; ++j) {
                                if (edge_scale != 0.0) {
                                    (*((*tempRootNode).connected_nodes + j)).edge /= edge_scale;
                                };
                            };
                        };    
                    };
                };
            };


            cnt = 0;
            sum = 0.0;
            min = 1e20;
            max = -1e20;
            mean = 0.0;
            sum_i = 0;
            min_i = count;
            max_i = 0;
            mean_i = 0;
            float v;
            for (i = 0; i < count; ++i) {
                tempRootNode = (root_nodes + i);                    
                conn_count = (*tempRootNode).count;
                if (conn_count > max_i) {
                    max_i = conn_count;
                };
                if (conn_count < min_i) {
                    min_i = conn_count;
                };
                cnt += conn_count;
                if (conn_count > 0) {
                    for (j = 0; j < conn_count; ++j) {
                        (*((*tempRootNode).connected_nodes + j)).edge *= matrix_scale;
                        v = (*((*tempRootNode).connected_nodes + j)).edge;
                        sum += v;
                        if (v < min) {
                            min = v;
                        };
                        if (v > max) {
                            max = v;
                        };
                    };
                };    
            };
            
            mean_i = float(cnt)/count;
            mean = sum/cnt;    
            //Calculate some statistics and report
            #if cout_output_enabled
            cout <<"\nInteractome statistics: \n";
            cout <<"Number of nodes: " << count << "\n";
            cout <<"Number of edges:"<<cnt << "\n";
            cout <<"\nOutgoing connections:\n";
            cout <<"Average number per node: "<<mean_i << "\n";
            cout <<"Min number per node: "<<min_i << "\n";
            cout <<"Max number per node: "<<max_i << "\n";
            cout <<"Total weight: "<<sum << "\n";
            cout <<"Average weight per node: "<<mean << "\n";
            cout <<"Min weight per node: "<<min << "\n";
            cout <<"Max weight per node: "<<max << "\n\n";
            #endif
        };    

        //Constructor with initialization

        Interactome(byte *buff, int size) {

            count = 0;
            root_nodes = NULL;
            root_nodes1 = NULL;
            root_nodes2 = NULL;
            genes_default = NULL;
            genes = NULL;
            method = 0;        // 'PageRank':0, 'SimpleFlow':1,'SteadyFlow':2,
            method1 = 0;
            method2 = 0;
            
            matrix_normalize = false;
            matrix_normalize_incoming = false;
            matrix_normalize_to_sum = false;
    
            matrix_scale = 0.0;
            large_address_size = false; //Use 16 bit  or 32 bit
            //bool byte_compressed_output;
            unique_ID = 0;
    
            iterations_limit = 0;
            eps = 0.0;
            init_ilimit = 0;
            init_eps = 0.0;
            zero_eps = 0.0;
            //PageRank
            c_const = 0.0;
            normalize = false;
            post_normalize = false;
            //SimpleFlow exclusive
            damping = 0.0;
            use_scaler = false;
            use_max = false;
            //SteadyFlow exclusive
            use_difference = false;
            use_relative_change = false;
            use_log_change = false;
            dt = 0.0;
            //RubberNet
            power_value = 1.0;
    
            sink_default = NULL;
            source_default = NULL;
            use_custom_sink_source = false;
    
            iterations_limit1 = 0;
            eps1 = 0.0;
            init_ilimit1 = 0;
            init_eps1 = 0.0;
            zero_eps1 = 0.0;
            //PageRank
            c_const1 = 0.0;
            normalize1 = false;
            post_normalize1 = false;
            //SimpleFlow exclusive
            damping1 = 0.0;
            use_scaler1 = false;
            use_max1 = false;
            //SteadyFlow exclusive
            use_difference1 = false;
            use_relative_change1 = false;
            use_log_change1 = false;
            dt1 = 0.0;
            //RubberNet
            power_value1 = 1.0;
    
            sink_default1 = NULL;
            source_default1 = NULL;
            genes_default1 = NULL;
    
    
            use_custom_sink_source1 = false;
    
    
            iterations_limit2 = 0;
            eps2 = 0.0;
            init_ilimit2 = 0;
            init_eps2 = 0.0;
            zero_eps2 = 0.0;
            //PageRank
            c_const2 = 0.0;
            normalize2 = false;
            post_normalize2 = false;
            //SimpleFlow exclusive
            damping2 = 0.0;
            use_scaler2 = false;
            use_max2 = false;
            //SteadyFlow exclusive
            use_difference2 = false;
            use_relative_change2 = false;
            use_log_change2 = false;
            dt2 = 0.0;
            //RubberNet
            power_value2 = 1.0;
    
            sink_default2 = NULL;
            source_default2 = NULL;
            genes_default2 = NULL;
            
            use_custom_sink_source2 = false;
    
            sink = NULL;
            source = NULL;
            weights = NULL;

            int i;
            int j;
            int inter_start_position;
            int start_i;
            int end_i;
            double sum;
            int conn_count;
            bool bi_direct;
            bool c_negative;
            float edge;
            int* indeces;
            int indeces_count = 0;
            byte c_data;
            #if cout_output_enabled
            cout << "Initializing interactome\n";
            #endif
            int buffer_position = 13;
            unique_ID = int(*(buff + 6)) | (int(*(buff + 7)) << 8) | (int(*(buff + 8)) << 16) | (int(*(buff + 9)) << 24);
            #if cout_output_enabled
            cout << "Unique ID in B: " << unique_ID << "\n";
            #endif
            
            byte settings = *(buff + 10);
            #if cout_output_enabled
            cout << "Settings: " << int(settings) <<"\n";
            #endif
            large_address_size = (settings & 1) > 0;
            //byte_compressed_output = (settings & 2) > 0;

            matrix_normalize = (settings & 4) > 0;
            matrix_normalize_incoming = (settings & 8) > 0;
            matrix_normalize_to_sum = (settings & 16) > 0;

            //method = int((settings & 0b11100000) >> 5);
            method1 = *(buff + 11);
            method2 = *(buff + 12);
            
            count = load_int(buff, &buffer_position, size, large_address_size);
            sink = new float [count];
            source = new float [count];
            weights = new float [count];
            //sink_default = new float [count];
            //source_default = new float [count];
            genes = new float [count];
            genes_default = new float [count];

            sink_default1 = new float [count];
            source_default1 = new float [count];
            sink_default2 = new float [count];
            source_default2 = new float [count];
            genes_default1 = new float [count];
            genes_default2 = new float [count];

            iterations_limit1 = load_int(buff, &buffer_position, size, false);
            iterations_limit2 = load_int(buff, &buffer_position, size, false);
            eps1 = load_float(buff, &buffer_position, size);
            eps2 = load_float(buff, &buffer_position, size);
            zero_eps1 = load_float(buff, &buffer_position, size);
            zero_eps2 = load_float(buff, &buffer_position, size);

            if (method1 == 0) {
            //PageRank settings
                c_const1 = load_float(buff, &buffer_position, size);
                settings = load_byte(buff, &buffer_position, size);
                normalize1 = (settings & 1) > 0;
                post_normalize1 = (settings & 2) > 0;

            } else if (method1 == 1) {
            //SimpleFlow settings
                damping1 = load_float(buff, &buffer_position, size);
                settings = load_byte(buff, &buffer_position, size);
                use_scaler1 = (settings & 1) > 0;
                use_max1 = (settings & 2) > 0;
            } else if (method1 == 2) {
            //SteadyFlow settings
                settings = load_byte(buff, &buffer_position, size);
                use_difference1 = (settings & 1) > 0;
                use_relative_change1 = (settings & 2) > 0;
                use_log_change1 = (settings & 4) > 0;
                use_custom_sink_source1 = (settings & 8) > 0;    

                dt1 = load_float(buff, &buffer_position, size);
                init_eps1 = load_float(buff, &buffer_position, size);
                init_ilimit1 = load_int(buff, &buffer_position, size, true);

                if (use_custom_sink_source1) {
                    for (i = 0; i < count; ++i) {
                        *(sink_default1 + i) = load_float(buff, &buffer_position, size);
                    }
                    for (i = 0; i < count; ++i) {
                        *(source_default1 + i) = load_float(buff, &buffer_position, size);
                    }
                } else {
                    float def_sink = load_float(buff, &buffer_position, size); 
                    float def_source = load_float(buff, &buffer_position, size); 
                    for (i = 0; i < count; ++i) {
                        *(sink_default1 + i) = def_sink;
                        *(source_default1 + i) = def_source;
                    }
                };

            } else if (method1 == 3) {
            //RubberNet method
                settings = load_byte(buff, &buffer_position, size);
                use_log_change1 = (settings & 1) > 0;
                use_scaler1 =  (settings & 2) > 0;
                power_value1 = load_float(buff, &buffer_position, size); 
            };



            //---------------------------------------------------------------------------------------

            if (method2 == 0) {
            //PageRank settings
                c_const2 = load_float(buff, &buffer_position, size);
                settings = load_byte(buff, &buffer_position, size);
                normalize2 = (settings & 1) > 0;
                post_normalize2 = (settings & 2) > 0;

            } else if (method2 == 1) {
            //SimpleFlow settings
                damping2 = load_float(buff, &buffer_position, size);
                settings = load_byte(buff, &buffer_position, size);
                use_scaler2 = (settings & 1) > 0;
                use_max2 = (settings & 2) > 0;

            } else if (method2 == 2) {
            //SteadyFlow settings

                settings = load_byte(buff, &buffer_position, size);
                use_difference2 = (settings & 1) > 0;
                use_relative_change2 = (settings & 2) > 0;
                use_log_change2 = (settings & 4) > 0;
                use_custom_sink_source2 = (settings & 8) > 0;    
                dt2 = load_float(buff, &buffer_position, size);
                init_eps2 = load_float(buff, &buffer_position, size);
                init_ilimit2 = load_int(buff, &buffer_position, size, true);

                if (use_custom_sink_source2) {
                    for (i = 0; i < count; ++i) {
                        *(sink_default2 + i) = load_float(buff, &buffer_position, size);
                    }
                    for (i = 0; i < count; ++i) {
                        *(source_default2 + i) = load_float(buff, &buffer_position, size);
                    }
                } else {
                    float def_sink = load_float(buff, &buffer_position, size); 
                    float def_source = load_float(buff, &buffer_position, size); 
                    for (i = 0; i < count; ++i) {
                        *(sink_default2 + i) = def_sink;
                        *(source_default2 + i) = def_source;
                    }
                };

            } else if (method2 == 3) {
            //RubberNet method
                settings = load_byte(buff, &buffer_position, size);
                use_log_change2 = (settings & 1) > 0;
                use_scaler2 =  (settings & 2) > 0;
                power_value2 = load_float(buff, &buffer_position, size); 
            };


            //---------------------------------------------------------------------------------------

            for (i = 0; i < count; ++i) {
                *(weights + i) = load_float(buff, &buffer_position, size);
            }

            matrix_scale = load_float(buff, &buffer_position, size); 
            #if cout_output_enabled
                cout << "large address size: " <<large_address_size <<"\n";
                //cout << "byte compressed output: " <<byte_compressed_output <<"\n";
                cout << "method1: " <<method1 <<"\n";
                cout << "method2: " <<method2 <<"\n";
                cout << "Gene count: " <<count <<"\n";
            #endif
            #if cout_output_enabled
                set_settings(1);
                cout << "Setting for subset1:\n";
                cout << "iterations limit: " <<iterations_limit <<"\n";
                cout << "eps: " <<eps <<"\n";
                cout << "init iterations limit: " <<init_ilimit <<"\n";
                cout << "init eps: " <<init_eps <<"\n";
                cout << "dt: " <<dt <<"\n";
            #endif
            //cout << "sink[0]: " << *sink << "\n";
            //cout << "source[0]: " << *source << "\n";

            #if cout_output_enabled
                set_settings(2);
                cout << "Setting for subset2:\n";
                cout << "iterations limit: " <<iterations_limit <<"\n";
                cout << "eps: " <<eps <<"\n";
                cout << "init iterations limit: " <<init_ilimit <<"\n";
                cout << "init eps: " <<init_eps <<"\n";
                cout << "dt: " <<dt <<"\n";
                cout << "\n";
                cout << "matrix_scale: " << matrix_scale <<"\n"; 
                cout << "matrix_normalize: " <<matrix_normalize << "\n";
                cout << "matrix_normalize_incoming: " <<matrix_normalize_incoming << "\n";
                cout << "matrix_normalize_to_sum: "  <<matrix_normalize_to_sum << "\n";
            #endif
            
            root_nodes1 = new struct RootNode [count];
            root_nodes2 = new struct RootNode [count];
            for (i = 0; i < count; ++i) {
                (*(root_nodes1 + i)).count = 0;
                (*(root_nodes2 + i)).count = 0;
            };
            #if cout_output_enabled
                cout << "Root nodes created: " << count << "\n";
            #endif
            //pre-allocate memory for full interactome
            inter_start_position = buffer_position;
            while (buffer_position < size) {
                #if cout_output_enabled
                    cout << "buffer_position " <<buffer_position << " size " << size << "\n";
                #endif
                start_i = load_int(buff, &buffer_position, size, large_address_size);
                #if cout_output_enabled
                    cout << "start_i " << start_i << "\n";
                #endif
                conn_count = load_int(buff, &buffer_position, size, large_address_size);
                #if cout_output_enabled
                    cout << "conn_count " << conn_count << "\n";
                #endif
                (*(root_nodes1 + start_i)).count += conn_count;
                (*(root_nodes2 + start_i)).count += conn_count;
                    for (i = 0; i < conn_count; ++i) {
                        end_i = load_int(buff, &buffer_position, size, large_address_size);
                        //cout << "end_i " << end_i;
                        c_data = load_byte(buff, &buffer_position, size);
                        //cout << "c_data" << c_data;
                        bi_direct = (c_data & 1) > 0;
                        if (bi_direct) {
                            (*(root_nodes1 + end_i)).count += 1;
                            (*(root_nodes2 + end_i)).count += 1;
                        };
                    };
            };
            #if cout_output_enabled
                cout << "node counts obtained\n";
            #endif
            

            int total_connections = 0;

            for (i = 0; i < count; ++i) {
                if ((*(root_nodes1 + i)).count > 0) {
                    (*(root_nodes1 + i)).connected_nodes = new struct Node [(*(root_nodes1 + i)).count];
                };
                if ((*(root_nodes2 + i)).count > 0) {
                    (*(root_nodes2 + i)).connected_nodes = new struct Node [(*(root_nodes2 + i)).count];
                };
                total_connections += (*(root_nodes1 + i)).count;

                if (method1 == 2) {
                    *(genes_default1 + i) = 0.95 + float(rand()) / RAND_MAX * 0.1;
                }
                else {
                    *(genes_default1 + i) = 0.0;
                };

                if (method2 == 2) {
                    *(genes_default2 + i) = 0.95 + float(rand()) / RAND_MAX * 0.1;
                }
                else {
                    *(genes_default2 + i) = 0.0;
                };
            };
            #if cout_output_enabled
                cout << "root nodes allocated. total connections: \n" <<total_connections << "\n";
            #endif

            //temporary progression counts
            bool iii = false;
            try {
                indeces = new int [count];
                iii = true;
                for (i = 0; i < count; ++i) {
                    *(indeces + i) = 0;
                };
                struct Node *tempNode;
                //load interactome
                buffer_position = inter_start_position;
                while (buffer_position < size) {
                    start_i = load_int(buff, &buffer_position, size, large_address_size);
                    conn_count = load_int(buff, &buffer_position, size, large_address_size);
                        for (i = 0; i < conn_count; ++i) {
                            end_i = load_int(buff, &buffer_position, size, large_address_size);
                            c_data = load_byte(buff, &buffer_position, size);
                            bi_direct = (c_data & 1) > 0;
                            c_negative = (c_data & 2) > 0;
                            if ((c_data & 4) > 0) {
                                edge = load_float(buff, &buffer_position, size);  
                            } else {
                                c_data = c_data >> 3;
                                edge = 1.0 / 31.0 * c_data;
                                if (c_negative) {
                                    edge = -edge;
                                };
                            };
    
                            tempNode = ((*(root_nodes1 + start_i)).connected_nodes + *(indeces + start_i));
                            (*tempNode).node_index = end_i;
                            (*tempNode).edge = edge;
                            tempNode = ((*(root_nodes2 + start_i)).connected_nodes + *(indeces + start_i));
                            (*tempNode).node_index = end_i;
                            (*tempNode).edge = edge;
                            *(indeces + start_i) += 1;
                            if (bi_direct) {
                                tempNode = ((*(root_nodes1 + end_i)).connected_nodes + *(indeces + end_i));
                                (*tempNode).node_index = start_i;
                                (*tempNode).edge = edge;
                                tempNode = ((*(root_nodes2 + end_i)).connected_nodes + *(indeces + end_i));
                                (*tempNode).node_index = start_i;
                                (*tempNode).edge = edge;
                                *(indeces + end_i) += 1;
                            };
                        };
                };
            }
            catch (...) {
                if (iii) {
                    delete [] indeces;
                };
                #if cout_output_enabled
                cout << "Error in B initialization...";
                #endif
                throw "Error in B initialization";
            };
            #if cout_output_enabled
            cout << "B initialized\n";
            #endif
            set_settings(1);
            normalize_matrix();
            set_settings(2);
            normalize_matrix();
            #if cout_output_enabled
            cout << "matrix normalized\n";
            #endif

            delete [] indeces;
            struct RootNode *tempRootNode;

            //Prepare interactome here
            
            
            
            if ((method1 == 0) && normalize1) { //Normalize probability for RandomWalk
            
                for (i = 0; i < count; ++i) {
                    sum = 0.0;
                    tempRootNode = (root_nodes1 + i);                    
                    conn_count = (*tempRootNode).count;
                    if (conn_count > 0) {
                        for (j = 0; j < conn_count; ++j) {
                            sum += (*((*tempRootNode).connected_nodes + j)).edge;
                        };
                        for (j = 0; j < conn_count; ++j) {
                            if (sum != 0.0) {
                                (*((*tempRootNode).connected_nodes + j)).edge /= sum;
                            };
                        };
                    };    
                };
            } else if (method1 == 2) {
                set_settings(1);
                initialize_from_default();
                propagate_SteadyFlow(init_eps, init_ilimit);
                for (i = 0; i < count; ++i) {
                    *(genes_default + i) = *(genes + i);
                };
            }
            
            if ((method2 == 0) && normalize2) { //Normalize probability for RandomWalk
            
                for (i = 0; i < count; ++i) {
                    sum = 0.0;
                    tempRootNode = (root_nodes2 + i);                    
                    conn_count = (*tempRootNode).count;
                    if (conn_count > 0) {
                        for (j = 0; j < conn_count; ++j) {
                            sum += (*((*tempRootNode).connected_nodes + j)).edge;
                        };
                        for (j = 0; j < conn_count; ++j) {
                            if (sum != 0.0) {
                                (*((*tempRootNode).connected_nodes + j)).edge /= sum;
                            };
                        };
                    };    
                };
            } else if (method2 == 2) {
                set_settings(2);
                initialize_from_default();
                propagate_SteadyFlow(init_eps, init_ilimit);
                for (i = 0; i < count; ++i) {
                    *(genes_default + i) = *(genes + i);
                };
            }
            
            
            #if cout_output_enabled
                cout << "Interactome prepared";
            #endif
            
        };


        //Destructor
        ~Interactome () {
            delete [] weights;
            delete [] sink;
            delete [] source;
            //delete [] sink_default;
            //delete [] source_default;

            delete [] sink_default1;
            delete [] source_default1;
            delete [] sink_default2;
            delete [] source_default2;
            delete [] genes_default1;
            delete [] genes_default2;


            delete [] genes;
            //delete [] genes_default;

            if (count > 0) {
                int i;
                for (i = 0; i < count; ++i) {
                    if ((*(root_nodes1 + i)).count > 0) {
                        delete [] (*(root_nodes1 + i)).connected_nodes;
                    };
                    if ((*(root_nodes2 + i)).count > 0) {
                        delete [] (*(root_nodes2 + i)).connected_nodes;
                    };
                };
            delete [] root_nodes1;            
            delete [] root_nodes2;            
            };
            #if cout_output_enabled    
            cout <<"Interactome destroyed successfully\n";
            #endif

        };

};


class Results_Stack {
    public:
        int results_count;

        float *correlations;
        float **set1_genes;
        float **set2_genes;
        bool **selections;

        int max_results;
        bool anticorrelations;
        int gene_count;
        int selection_length;

        Results_Stack (int max_corr) {
            results_count = 0;
            if (max_corr >= 0) {
                max_results = max_corr;
                anticorrelations = false;
            } else {
                max_results = -max_corr;
                anticorrelations = true;
            };
            correlations = new float [max_results];
            set1_genes = new float* [max_results];
            set2_genes = new float* [max_results];
            selections = new bool* [max_results];
        };

        int insertion_index(float corr) {
            int result = 0;
            if (anticorrelations) {
                while ((result < results_count) && (*(correlations + result) <= corr)) {
                    result += 1;
                }
            } else {
                while ((result < results_count) && (*(correlations + result) >= corr)) {
                    result += 1;
                }
            };
            return result;
        };

        void add_result(bool *selection, int s_count, float *genes1, float *genes2, int g_count, float corr) {
            int i;
            int j;
            int index = insertion_index(corr);
            float *g1;
            float *g2;
            bool *s;
            selection_length = s_count;

            if (results_count < max_results) {
                *(set1_genes + results_count) = new float [g_count];
                *(set2_genes + results_count) = new float [g_count];
                *(selections + results_count) = new bool [s_count];    
                results_count += 1;
            }

            if (index < results_count) {
                g1 = *(set1_genes + results_count - 1);
                g2 = *(set2_genes + results_count - 1);
                s = *(selections + results_count - 1);

                for (i = results_count - 1; i > index; i--) {
                    *(set1_genes + i) = *(set1_genes + i - 1);
                    *(set2_genes + i) = *(set2_genes + i - 1);
                    *(selections + i) = *(selections + i - 1);
                    *(correlations + i) = *(correlations + i - 1);
                }
                *(set1_genes + index) = g1;
                *(set2_genes + index) = g2;
                *(selections + index) = s;
                *(correlations + index) = corr;
                float_copy(genes1, g1, g_count);
                float_copy(genes2, g2, g_count);
                bool_copy(selection, s, s_count);
            }
            #if cout_output_enabled
            cout << "Results count: " << results_count << "\n";
            cout << "Best correlation: " << *(correlations) << "\n";
            cout << "Worst correlation: " << *(correlations + results_count - 1) << "\n";
            #endif
        };


        void print_results() {
            #if cout_output_enabled
            cout << "Results count: " << results_count << "\n";
            cout << "Selection length: " << selection_length <<"\n";
            int i;
            int j;
            for (i = 0; i < results_count; ++i) {
                cout << i+1 <<". Correlation "<< *(correlations + i) << "  Selection: ";
                for (j = 0; j < selection_length; ++j) {
                    cout << *(*(selections + i) + j);
                }
                cout << "\n";
            };
            #endif
        };

        ~Results_Stack() {
            int i;
            if (results_count > 0) {
                for (i = 0; i < results_count; ++i) {
                    delete [] *(set1_genes + i);
                    delete [] *(set2_genes + i);
                    delete [] *(selections + i);
                };
            };
            delete [] correlations;
            delete [] set1_genes;
            delete [] set2_genes;
            delete [] selections;
            #if cout_output_enabled
            cout <<"Results_Stack destroyed successfully\n";
            #endif
        };
};






class QuerySet {
    public:
        Interactome *interactome;
    
        int unique_ID;
        int unique_ID_B;
        int task_subindex;
        int count;
        struct Sample *samples;
        bool large_address_size; //Use 16 bit  or 32 bit
        bool return_full_vectors;
        bool return_cross_correlations;
        bool all_combinations;
        bool return_byte_scaled;
        byte batch2_combi;
        int subset1;
        int subset2;
        byte query_settings;
        int max_correlations;
        int n_combinations;
        int combination_generation_count;
        
        int starter_selection_size;
        bool *starter_selection;


    //Constructor
    QuerySet(Interactome *inter, byte *buff, int size) {


            interactome  = NULL;
        
            unique_ID = 0;
            unique_ID_B = 0;
            task_subindex = 0;
            count = 0;
            samples = NULL;
            large_address_size = false; //Use 16 bit  or 32 bit
            return_full_vectors = false;
            return_cross_correlations = false;
            all_combinations = false;
            return_byte_scaled = false;
            batch2_combi = 0;
            subset1 = 0;
            subset2 = 0;
            query_settings = 0;
            max_correlations = 0;
            n_combinations = 0;
            combination_generation_count = 0;
            
            starter_selection_size = 0;
            starter_selection = NULL;

            interactome = inter;
            //Initialize data here
            int i;
            int j;
            int mut_count;
            starter_selection_size = 0;
            
            #if cout_output_enabled
                cout << "Initializing sample A string set\n";
            #endif
            
            int buffer_position = 9;
            unique_ID = int(*(buff + buffer_position)) | (int(*(buff + buffer_position + 1)) << 8) | (int(*(buff + buffer_position + 2)) << 16) | (int(*(buff + buffer_position + 3)) << 24);
            unique_ID_B = int(*(buff + buffer_position + 4)) | (int(*(buff + buffer_position + 5)) << 8) | (int(*(buff + buffer_position + 6)) << 16) | (int(*(buff + buffer_position + 7)) << 24);
            buffer_position += 8;
            
            task_subindex = load_int(buff, &buffer_position, size, true);
            
            #if cout_output_enabled
                cout << "Unique ID in A: " << unique_ID << "\n";
                cout << "Unique ID in B: " << unique_ID_B << "\n";
                cout << "Task subindex: " << task_subindex << "\n";
            #endif
            
            if ((unique_ID_B & 1) > 0) {
                if (unique_ID_B != (*interactome).unique_ID) {
                    #if cout_output_enabled
                        cout <<"Unique ID B does not match Unique ID of interactome! " <<unique_ID_B << " <> " << (*interactome).unique_ID << " !\n";
                    #endif
                    throw "Unique ID B does not match Unique ID of interactome! ";
                };
            };
            
            byte settings;
            settings = load_byte(buff, &buffer_position, size);
            query_settings = settings;
            large_address_size = (settings & 1) > 0;
            return_full_vectors = (settings & 2) > 0;
            return_cross_correlations = (settings & 4) > 0;
            all_combinations = (settings & 8) > 0;
            return_byte_scaled = (settings & 16) > 0;

            if ((all_combinations == false) && (return_cross_correlations == false)) {
                return_full_vectors = true;
            };

            batch2_combi = ((settings & 0b11100000) >> 5) + 1;
            subset1 = load_int(buff, &buffer_position, size, false);
            subset2 = load_int(buff, &buffer_position, size, false);
            count = int(subset1) + int(subset2);
            max_correlations = 0;

            if (return_cross_correlations && all_combinations) {
                max_correlations = load_signed_int(buff, &buffer_position, size, false);
                n_combinations   = load_signed_int(buff, &buffer_position, size, large_address_size);
                if (n_combinations > 0) {
                    load_compressed_selection(buff, &buffer_position, size);
                };
            };
            #if cout_output_enabled
                cout << "large address size: " <<large_address_size <<"\n";
                cout << "return_full_vectors: " <<return_full_vectors <<"\n";
                cout << "return cross: " <<return_cross_correlations <<"\n";
                cout << "All combi: " <<all_combinations <<"\n";
                cout << "format" <<int(settings) <<"\n";
                cout << "batch2: " <<int(batch2_combi) <<"\n";
                cout << "subset1: " <<int(subset1) <<"\n";
                cout << "subset2:" <<int(subset2) << "\n";
                cout << "count" << count << "\n";
                cout << "max_corr" << max_correlations <<"\n"; 
            #endif

            
            struct Sample *tmpSample;
            struct Mutation *tmpMutation; 

            samples = new struct Sample [count];
            for (i = 0; i < count; ++i) {
                tmpSample = (samples + i);
                (*tmpSample).count = 0;

            };
            for (i = 0; i < count; ++i) {
                tmpSample = (samples + i);
                //(*tmpSample).count = 0;

                mut_count = load_signed_int(buff, &buffer_position, size, large_address_size);
                #if cout_output_enabled
                    cout << "Mutcount for sample "<<i+1 << " is " << mut_count <<"\n";
                #endif    
                if (mut_count == -1) {
                    //Get full set of floats
                    mut_count = (*interactome).count;
                    (*tmpSample).count = mut_count;
                    (*tmpSample).mutations = new struct Mutation [mut_count];
                    for (j = 0; j < mut_count; ++j) {
                          tmpMutation = ((*tmpSample).mutations + j);
                          (*tmpMutation).position = j;
                          (*tmpMutation).value = load_float(buff, &buffer_position, size);
                    };   
                } else if (mut_count == -2) {
                    //Get full set of bits
                    mut_count = (*interactome).count;
                    (*tmpSample).count = mut_count;
                    (*tmpSample).mutations = new struct Mutation [mut_count];

                    bool *bitarray;
                    try {
                        bitarray = decode_bit_compressed(buff, &buffer_position, mut_count);
                    
                        for (j = 0; j < mut_count; ++j) {
                              tmpMutation = ((*tmpSample).mutations + j);
                              (*tmpMutation).position = j;
                              if (*(bitarray + j)) {
                                  (*tmpMutation).value = 1.0;
                              } else {
                                  (*tmpMutation).value = 0.0;
                              };
                        };
                    }
                    catch (...) {
                        delete [] bitarray;
                        #if cout_output_enabled
                            cout <<"Error reading bitarray...\n";
                        #endif
                        throw "Error reading bitarray";
                    };
                    delete [] bitarray;


                } else {
                    //Get sparse    
                     (*tmpSample).count = mut_count;
                     if (mut_count > 0) {
                         (*tmpSample).mutations = new struct Mutation [mut_count];
                         for (j = 0; j < mut_count; ++j) {
                              tmpMutation = ((*tmpSample).mutations + j);
                              (*tmpMutation).position = load_signed_int(buff, &buffer_position, size, large_address_size);
                            //if negative - followed by float - otherwise assume value of 1.0
                              if  ((*tmpMutation).position < 0) {
                                    (*tmpMutation).position = -((*tmpMutation).position);
                                    (*tmpMutation).value = load_float(buff, &buffer_position, size);
                              } else {
                                    (*tmpMutation).value = 1.0;    
                              };
                         };   
                     };
                };
            };

    };

    ~QuerySet() {
          //Free memory here....
          int i;
          if (starter_selection_size > 0) {
               delete [] starter_selection;
          };
          if (count > 0) {
                int i;
                for (i = 0; i < count; ++i) {
                    if ((*(samples + i)).count > 0) {
                        delete [] (*(samples + i)).mutations;
                    };
                };
            delete [] samples;            
            };
        
        interactome = NULL;
        #if cout_output_enabled
            cout <<"QuerySet destroyed successfully\n";
        #endif
    };



    bool generator_next_combination(bool *selection, int count, byte max_on) {
        int i;
        int j;
        int offs;
        bool result;

        combination_generation_count += 1;

        if ((n_combinations > 0) && (combination_generation_count > n_combinations)) {
            #if cout_output_enabled
                cout <<"Combinations limit reached. Terminating..." << combination_generation_count -1 << "\n";
            #endif
            return false;
        };

        //Try move
        result = false;
        for (i = 1; i < count; i++) {
            if ((*(selection + i)) && (!(*(selection + i - 1)))) {
                *(selection + i) = false;
                *(selection + i - 1) = true;
                offs = i - 2;
                for (j = i - 2; j >= 0; j--) {
                    if (*(selection + j)) {
                         *(selection + j) = false;
                         *(selection + offs) = true;
                         offs -= 1;
                    };
                };
                result = true;
                break;    
            };
        };
    
        if (!result) {
            j = 0;
            for (i = 0; i < count; i++) {
                if (*(selection + i)) {
                    j += 1;
                }
            };
            if ((j < max_on) && (j < count)) {
                for (i = 0; i < count; i++) {
                    *(selection + i) = i >= (count - 1 - j);
                };
                result = true;
            };
        };
    
        return result;
    };



    void load_compressed_selection(byte *buff, int *buffer_position, int size) {
        int i;
        int j;
        byte val;
        byte mask;
        j = int(subset2)/8;
        if (subset2 % 8 != 0) {
            j += 1;
        };
        
        starter_selection_size = subset2;
        starter_selection = new bool [starter_selection_size];
        #if cout_output_enabled
            cout <<"Loaded state  (len =  " << j << ")\n";
        #endif

        for (i = 0; i < subset2; ++i) {
            val = *(buff + *buffer_position + i/8);
            mask = (1 << (i%8));
            *(starter_selection + i) = (val & mask) > 0;
            #if cout_output_enabled
                cout << *(starter_selection + i);
            #endif
        };
        #if cout_output_enabled
            cout << "\n";
        #endif
        *buffer_position += j;
    };

    void compress_selection(byte **compressed_selections, int *compressed_selection_lengths, int index, bool *selection) {
        int bmin = count / 8;
        if (count % 8 > 0) {
            bmin += 1;
        };
        (*(compressed_selections + index)) = new byte [bmin];
         *(compressed_selection_lengths + index) = bmin;
        byte *tmp = *(compressed_selections + index);
        int i;
        int j;
        byte in;
        for (i = 0; i < bmin; ++i) {
            *(tmp + i) = 0;
        };
        for (i = 0; i < count; ++i) {
            if (*(selection + i)) {
                in = i % 8;    
                j = i / 8; 
                *(tmp + j) += (1 << in);              
            };
        };
    };


    struct Mutation* accumulate_vector(bool *selection, int *vector_length) {
        struct Mutation* result;
        int i;
        int j;
        int cnt;
        int cc;
        cnt = 0;
        for (i = 0; i < count; ++i) {
            if (*(selection + i)) {
                cnt += (*(samples + i)).count;        
            };
        };
        #if cout_output_enabled
            cout << "Selection: \n";
            for (i = 0; i < count; ++i) {
                cout << (*(selection + i));
            };
            cout << "\n";
        #endif

        *vector_length = cnt;
        int indx = 0;
        if (cnt > 0) {
            result = new struct Mutation [cnt];
            for (i = 0; i < count; ++i) {
                if (*(selection + i)) {
                    cc = (*(samples + i)).count;
                    for (j = 0; j < cc; ++j) {
                        *(result + indx) = *((*(samples + i)).mutations + j);
                        indx += 1;
                    };                    
                };
            };
        }
        
        return result;
    };
    
    

    void compress_vector(byte **compressed_vectors, int *compressed_vector_lengths, float *genes, int index) {
        int i;
        int cnt;
        cnt = 0;
        int l_index;
        int l_val;
        int gene_count = (*interactome).count;
        float zeps = (*interactome).zero_eps;
        #if cout_output_enabled    
            cout << "Return Byte Scaled " <<return_byte_scaled << "\n";
        #endif
        /*float minvv = 1e20;
        float maxvv = 1e-20;
        */
        //cout << "zero_eps: " << zeps <<"\n";
        //Count non-zero genes to be stored.
        for (i = 0; i < gene_count; ++i) {
            if (abs_value(*(genes + i)) > zeps) {
                cnt += 1;        
            };
            /*if (minvv > *(genes + i)) {
                minvv = *(genes + i);
            };
            if (maxvv < *(genes + i)) {
                maxvv = *(genes + i);
            };
            */

        };
        //cout <<"Nonzero: " << cnt << " Min: " <<minvv << " Max:" <<maxvv <<  "\n";
        //predict sizes
        //size of counts/indexes    
        if (large_address_size) {
            l_index = 4;
        } else {
            l_index = 2;
        };
        //size of returned values    
        if (return_byte_scaled) {
            l_val = 1;
        } else {
            l_val = 4;
        };

        int sparse_len = l_index + (l_index + l_val) * cnt;
        int full_len = l_val * gene_count;
        bool use_sparse = sparse_len < full_len;
        //cout <<"sparse " <<use_sparse <<"\n";
        //cout <<"return byte " <<return_byte_scaled <<"\n";

        int datalen;

        if (use_sparse) {
            datalen = sparse_len;
        } else {
            datalen = full_len;
        };
        //Add scalings and offset if return as byte
        if (return_byte_scaled) {
            datalen += 8;
        };
        datalen += 1; //for format byte
        
        //Allocating memory for output compressed vector
        *(compressed_vectors + index) = new byte [datalen];
        *(compressed_vector_lengths + index) = datalen;
        
        byte *data_vect;
        data_vect = *(compressed_vectors + index);
        int data_position = 0;
        byte settings;
        settings = 0;
        if (use_sparse) {
            settings += 1;
        };
        if (return_byte_scaled) {
            settings += 2;
        };
        write_byte(data_vect, &data_position, settings);
        
        float minv;
        float maxv;
        
        if (return_byte_scaled) {
            minv = 1e20;
            maxv = -1e20;
            for (i = 0; i < gene_count; ++i) {
                if ((abs_value(*(genes + i)) > zeps) || (!use_sparse)) {
                    if (*(genes + i) > maxv) {
                        maxv = *(genes + i);    
                    };
                    if (*(genes + i) < minv) {
                        minv = *(genes + i);    
                    };
                };
            };
            write_float(data_vect, &data_position, minv); //scaling
            write_float(data_vect, &data_position, maxv); //scaling
            //cout <<"minv " << minv <<"\n";
            //cout <<"maxv " << maxv <<"\n";
        };
        
        float d_val;
        byte b_val;
        if (use_sparse) {
            write_index(data_vect, &data_position, cnt, large_address_size);
        }
        for (i = 0; i < gene_count; ++i) {
            if ((abs_value(*(genes + i)) > zeps) || (!use_sparse)) {
                if (use_sparse) {
                    write_index(data_vect, &data_position, i, large_address_size);
                };
                d_val = *(genes + i);
                if (return_byte_scaled) {
                    if ((maxv - minv) > 0.0) {
                        d_val = (d_val - minv)/(maxv - minv) * 255;
                        b_val = byte(d_val);
                    } else {
                        b_val = 255;
                    };
                    write_byte(data_vect, &data_position, b_val);

                } else {
                    write_float(data_vect, &data_position, d_val);
                };
            };
        };


    };


    void save_bytes(byte *src, int length, byte *dest, int *buffer_position) {
        int i;
        for (i = 0; i < length; ++i) {
            *(dest + i + *buffer_position) = *(src + i);
        };
        *buffer_position = *buffer_position + length;
    };



    void process_query(byte** result, int *result_line_size) {
        int size;
        bool *selection;
        int selection_count = 0;
        bool *combi_selection;
        int combi_selection_count = 0;
        int i;
        int j;
        int buffer_position;
        struct Mutation *vector;
        int vector_length = 0;
        float *gene_set1;
        int gene_set1_count = 0;
        float *gene_set2;
        int gene_set2_count = 0;
        selection = new bool [count];
        selection_count = count;        
        set_bool(selection, count, false);
        int gene_count = (*interactome).count;
        int subselection;    
        float **result_vectors;
        bool results_allocated = false;
        byte **compressed_vectors;
        int *compressed_vector_lengths;    
        byte *compressed_selection;
        int compressed_selection_length = 0;
        byte **compressed_selections;
        int *compressed_selection_lengths;
        float *correlations;

        float **gene_blockset1;
        int gene_blockset_count = 0;

        int results_count = 0;    

        Results_Stack *results_stack;


        time_t start_time;
        time_t end_time;
        time(&start_time);



        if (return_cross_correlations) {
            gene_set1 = new float [gene_count];
            gene_set1_count = gene_count;
            gene_set2 = new float [gene_count];
            gene_set2_count = gene_count;
            combi_selection = new bool [count];
            combi_selection_count = count;


            if (all_combinations) {
                    // Implement all combinations with max correlations stored here.....
                    results_stack = new Results_Stack (max_correlations);

                    try {    
                        if (subset1 > 0) {
                            (*interactome).set_settings(1);
                            gene_blockset1 = new float* [subset1];
                            for (i = 0; i < subset1; ++i) {
                                *(gene_blockset1 + i) = new float [gene_count];
                            };
                            gene_blockset_count = subset1;
                        
                            #if cout_output_enabled
                                cout <<"Propagating subset 1 ... \n";
                            #endif
                            for (i = 0; i < subset1; ++i) {
                                #if cout_output_enabled
                                    cout <<"Sample " << i+1 <<" of " << int(subset1) << "\n";
                                #endif
                                set_bool(selection, count, false);
                                *(selection + i) = true;
                                vector = accumulate_vector(selection, &vector_length);
                                (*interactome).propagate(vector, vector_length);
                                float_copy((*interactome).genes, *(gene_blockset1 + i), gene_count);
                                if (vector_length > 0) {
                                    delete [] vector;
                                    vector_length = 0;
                                };
                            };
                        };
                        set_bool(selection, count, false);
                    
                        combination_generation_count = 0;            
                        
                        if (n_combinations > 0) {
                            bool_copy(starter_selection, (selection + subset1), starter_selection_size);
                        };
                        //Do combinations on set 2...
                        #if cout_output_enabled
                            cout << "Considering combinations from set2...\n";
                        #endif
                        (*interactome).set_settings(2);
                        while (generator_next_combination((selection + subset1), subset2, batch2_combi)) {
                            vector = accumulate_vector(selection, &vector_length);
                            (*interactome).propagate(vector, vector_length);      
                            if (subset1 > 0) {
                                for (i = 0; i < subset1; ++i) {
                                    set_bool(selection, subset1, false);
                                    *(selection + i) = true;
                                    (*results_stack).add_result(selection, count, 
                                                                *(gene_blockset1 + i), 
                                                                (*interactome).genes, 
                                                                gene_count, 
                                                                correlation(*(gene_blockset1 + i), 
                                                                           (*interactome).genes, 
                                                                            (*interactome).weights, 
                                                                            gene_count)
                                                                );
                                    *(selection + i) = false;
    
                                };
                            } else {
                                (*results_stack).add_result(selection, count, (*interactome).genes, (*interactome).genes, gene_count, 1.0);
                            };
                            if (vector_length > 0) {
                                delete [] vector;
                                vector_length = 0;
                            };
    
                        };
    
                        //Compress results
                        results_count = (*results_stack).results_count;
    
                        compressed_selections = new byte* [results_count];
                        compressed_selection_lengths = new int [results_count];
                        if (return_full_vectors) {
                            if (subset1 > 0) {
                                compressed_vectors = new byte* [results_count * 2];
                                compressed_vector_lengths = new int [results_count *2];
                            } else {
                                compressed_vectors = new byte* [results_count];
                                compressed_vector_lengths = new int [results_count];
                            };
                        };

                        for (i = 0; i < results_count; ++i) {
                            if (return_full_vectors) {
                                if (subset1 > 0) {
                                    *(compressed_vector_lengths + i*2) = 0;
                                    *(compressed_vector_lengths + i*2 + 1) = 0;
                                } else {
                                    *(compressed_vector_lengths + i) = 0;
                                };
                            }
                            *(compressed_selection_lengths + i) = 0;
                        };     
    
                        results_allocated = true;
                       
                        for (i = 0; i < results_count; ++i) {
                            if (return_full_vectors) {
                                if (subset1 > 0) {
                                    compress_vector(compressed_vectors, compressed_vector_lengths, *((*results_stack).set1_genes + i), i*2);
                                    compress_vector(compressed_vectors, compressed_vector_lengths, *((*results_stack).set2_genes + i), i*2 + 1);
                                } else {
                                    compress_vector(compressed_vectors, compressed_vector_lengths, *((*results_stack).set1_genes + i), i);
                                };
                            }
                            compress_selection(compressed_selections, compressed_selection_lengths, i, *((*results_stack).selections + i) );
                        };     
    
                   }

                   catch (...) {
                        #if cout_output_enabled
                            cout << "Error in combinatorical search... \n";
                        #endif
                        delete [] combi_selection;
                        delete [] gene_set1;
                        delete [] gene_set2;
                        if (vector_length > 0) {
                            delete [] vector;
                        };

                        if (gene_blockset_count > 0) {
                            if (subset1 > 0) {
                                for (i = 0; i < subset1; ++i) {
                                    delete [] (*(gene_blockset1 + i));
                                };
                                delete [] gene_blockset1;
                            };
                        };    


                        //--------------------                        
                        if (results_allocated) {
                            if (return_full_vectors) {
                               if (subset1 > 0) {
                                    for (i = 0; i < results_count * 2; ++i) {
                                        if (*(compressed_vector_lengths + i) > 0) {
                                            delete [] *(compressed_vectors + i);
                                        };
                                    };
                                } else {
                                    for (i = 0; i < results_count; ++i) {
                                        if (*(compressed_vector_lengths + i) > 0) {
                                            delete [] *(compressed_vectors + i);
                                        };
                                    };
                                };
                                delete [] compressed_vectors;
                                delete [] compressed_vector_lengths;
                            };
                        
                            for (i = 0; i < results_count; ++i) {
                                if (*(compressed_selection_lengths + i) > 0) {
                                    delete [] *(compressed_selections + i);
                                };                
                            };
        
                            delete [] compressed_selections;     
                            delete [] compressed_selection_lengths; 
                        };
                        //--------------------
                        
                        delete results_stack;
                        throw "Error in combinatorial search";
                   };

                   // Generate results with results vectors here
                   size = 15 + 8 + 32 + 4 + 9; //IDs and combinations count, + 8 bytes for time elapsed. + hash + taskindex + header
                   //
                   for (i = 0; i < results_count; ++i) {
                        size += *(compressed_selection_lengths + i);
                        size += 4;//for correlation float
                   }; 
                   if (return_full_vectors) { 
                       if (subset1 > 0) {
                           for (i = 0; i < results_count * 2; ++i) {
                                size += *(compressed_vector_lengths + i);
                           }; 
                       } else {
                           for (i = 0; i < results_count; ++i) {
                                size += *(compressed_vector_lengths + i);
                           }; 
                       };
                   }; 

                   //Generate result buffer
                   //auto end = std::chrono::system_clock::now(); 
                   *result = new byte [size];   
                   *result_line_size = size;    
                
                   buffer_position = 0; 
                   *(*result + buffer_position + 0) = 13;
                   *(*result + buffer_position + 1) = 21;
                   *(*result + buffer_position + 2) = 6;
                   *(*result + buffer_position + 3) = 73;
                   *(*result + buffer_position + 4) = 17;
                   *(*result + buffer_position + 5) = 18;
                   *(*result + buffer_position + 6) = 80;
                   *(*result + buffer_position + 7) = 180;
                   *(*result + buffer_position + 8) = 210;
                   buffer_position += 9;

                   *(*result + buffer_position) = query_settings;
                   buffer_position += 1;

                   write_index(*result, &buffer_position, subset1, false);
                   write_index(*result, &buffer_position, subset2, false);

                   *(*result + buffer_position + 0) = unique_ID & 0b11111111;
                   *(*result + buffer_position + 1) = (unique_ID >> 8) & 0b11111111; 
                   *(*result + buffer_position + 2) = (unique_ID >> 16) & 0b11111111;  
                   *(*result + buffer_position + 3) = (unique_ID >> 24) & 0b11111111;   
                   *(*result + buffer_position + 4) = (*interactome).unique_ID & 0b11111111;
                   *(*result + buffer_position + 5) = ((*interactome).unique_ID >> 8) & 0b11111111; 
                   *(*result + buffer_position + 6) = ((*interactome).unique_ID >> 16) & 0b11111111;  
                   *(*result + buffer_position + 7) = ((*interactome).unique_ID >> 24) & 0b11111111;   
                   buffer_position += 8;
                   write_index(*result, &buffer_position, task_subindex, true);


                   
                   write_index(*result, &buffer_position, results_count, false);

                   time(&end_time);
                   #if global_time_recording_enabled 
                       double dtime = difftime(end_time, start_time);
                   #else 
                       double dtime = 0;
                   #endif 
                   write_double(*result, &buffer_position, dtime);
                   #if cout_output_enabled
                       cout << "Query processed in: " <<  dtime << " s\n";
                   #endif
                   for (i = 0; i < results_count; ++i) {
                       write_float(*result, &buffer_position, *((*results_stack).correlations + i)); 
                       save_bytes(*(compressed_selections + i), *(compressed_selection_lengths + i), *result, &buffer_position);
                       if (return_full_vectors){
                           if (subset1 > 0) { 
                               save_bytes(*(compressed_vectors + i*2), *(compressed_vector_lengths + i*2), *result, &buffer_position);
                               save_bytes(*(compressed_vectors + i*2 + 1), *(compressed_vector_lengths + i*2 + 1), *result, &buffer_position); 
                           } else {
                               save_bytes(*(compressed_vectors + i), *(compressed_vector_lengths + i), *result, &buffer_position);
                           } ;
                       };
                   };
                   #if cout_output_enabled
                   cout << "result buffer: " << buffer_position << " " <<  size << "\n";
                   #endif 
                   byte *sha;
                   sha = get_sha256(*result, buffer_position);
                   #if cout_output_enabled
                   for (i = 0; i < 32; i++) {
                     cout << int(sha[i]) <<',';
                   };
                   cout << "\n";
                   #endif 
                   save_bytes(sha, 32, *result, &buffer_position);
                   #if cout_output_enabled 
                   cout << "result buffer: " << buffer_position << " " <<  size << "\n";
                   #endif 

                   delete [] sha; 

                    //tidy up


                    if (return_full_vectors) {

                        if (subset1 > 0) {
                            for (i = 0; i < results_count * 2; ++i) {
                                if (*(compressed_vector_lengths + i) > 0) {
                                    delete [] *(compressed_vectors + i);
                                };
                            };
                        } else {
                            for (i = 0; i < results_count; ++i) {
                                if (*(compressed_vector_lengths + i) > 0) {
                                    delete [] *(compressed_vectors + i);
                                };
                            };
                        };
                        delete [] compressed_vectors;
                        delete [] compressed_vector_lengths;
                    };
                
                    for (i = 0; i < results_count; ++i) {
                        if (*(compressed_selection_lengths + i) > 0) {
                            delete [] *(compressed_selections + i);
                        };                
                    };

                    delete [] compressed_selections;     
                    delete [] compressed_selection_lengths; 
                    (*results_stack).print_results();    
                    delete results_stack;
                    if (subset1 > 0) {
                        for (i = 0; i < subset1; ++i) {
                            delete [] (*(gene_blockset1 + i));
                        };
                        delete [] gene_blockset1;
                    };



            } else {
                subselection = int(subset1);
                if (subset2 < subset1) {
                    subselection = int(subset2);
                };


                compressed_selections = new byte* [subselection];
                compressed_selection_lengths = new int [subselection];
                correlations = new float [subselection];
                if (return_full_vectors) {
                    compressed_vectors = new byte* [subselection * 2];
                    compressed_vector_lengths = new int [subselection *2];
                };
                compressed_selection_length = count;
                vector_length = 0;
                try {
                    
                    for (i = 0; i < subselection; ++i) {
                        set_bool(combi_selection, count, false);
                        //part 1
                        set_bool(selection, count, false);
                        *(selection + i) = true;
                        *(combi_selection + i) = true;
                        vector = accumulate_vector(selection, &vector_length);
                        (*interactome).set_settings(1);
                        (*interactome).propagate(vector, vector_length);
                        float_copy((*interactome).genes, gene_set1, gene_count);
                        if (return_full_vectors) {
                            *(compressed_vector_lengths + i*2) = 0;
                            compress_vector(compressed_vectors, compressed_vector_lengths, (*interactome).genes, i*2);
                        }
     
                        if (vector_length > 0) {
                           delete [] vector;
                           vector_length = 0;
                        };
                        //part 2
                        set_bool(selection, count, false);
                        *(selection + i + subset1) = true;
                        *(combi_selection + i + subset1) = true;
                        vector = accumulate_vector(selection, &vector_length);
                        (*interactome).set_settings(2);
                        (*interactome).propagate(vector, vector_length);
                        float_copy((*interactome).genes, gene_set2, gene_count);
                        if (return_full_vectors) {
                            *(compressed_vector_lengths + i*2 + 1) = 0;
                            compress_vector(compressed_vectors, compressed_vector_lengths, (*interactome).genes, i*2 + 1);
                        }
    
                        compress_selection(compressed_selections, compressed_selection_lengths, i, combi_selection);
    
                        *(correlations + i) = correlation(gene_set1, gene_set2, (*interactome).genes, gene_count);
                        #if cout_output_enabled
                        cout <<"Correlation " << *(correlations + i) << "\n";
                        #endif
                        if (vector_length > 0) {
                           delete [] vector;
                           vector_length = 0;
                        };
                    };     
                }
                catch(...) {
                    #if cout_output_enabled
                    cout <<"Error in processing...\n";
                    #endif
                        if (vector_length > 0) {
                           delete [] vector;
                           vector_length = 0;
                        };


                    if (return_full_vectors) {
                        for (i = 0; i < subselection * 2; ++i) {
                            if (*(compressed_vector_lengths + i) > 0) {
                                delete [] *(compressed_vectors + i);
                            };
                        };
                        delete [] compressed_vectors;
                        delete [] compressed_vector_lengths;
                    };
                
                    for (i = 0; i < subselection; ++i) {
                        if (*(compressed_selection_lengths + i) > 0) {
                            delete [] *(compressed_selections + i);
                        };                
                    };
                    delete [] compressed_selections;     
                    delete [] compressed_selection_lengths; 
                    delete [] correlations;

                    delete [] combi_selection;
                    delete [] gene_set1;
                    delete [] gene_set2;
        

                    throw "Error in processing";
                };
                   // Generate results with results vectors here
                   size = 15 + 8 + 32 + 4 + 9; //IDs and combinations count, + 8 bytes for time elapsed. + hash + taskindex + header


                   for (i = 0; i < subselection; ++i) {
                        size += *(compressed_selection_lengths + i);
                        size += 4;//for correlation float
                   }; 
                   if (return_full_vectors) { 
                       for (i = 0; i < subselection * 2; ++i) {
                            size += *(compressed_vector_lengths + i);
                       }; 
                   }; 
                    
                   //Generate result buffer
                   
                   
                   *result = new byte [size];   
                   *result_line_size = size;    
                
                   buffer_position = 0; 
                   *(*result + buffer_position + 0) = 13;
                   *(*result + buffer_position + 1) = 21;
                   *(*result + buffer_position + 2) = 6;
                   *(*result + buffer_position + 3) = 73;
                   *(*result + buffer_position + 4) = 17;
                   *(*result + buffer_position + 5) = 18;
                   *(*result + buffer_position + 6) = 80;
                   *(*result + buffer_position + 7) = 180;
                   *(*result + buffer_position + 8) = 210;
                   buffer_position += 9;

                   *(*result + buffer_position) = query_settings;
                   buffer_position += 1;

                   write_index(*result, &buffer_position, subset1, false);
                   write_index(*result, &buffer_position, subset2, false);

                   *(*result + buffer_position + 0) = unique_ID & 0b11111111;
                   *(*result + buffer_position + 1) = (unique_ID >> 8) & 0b11111111; 
                   *(*result + buffer_position + 2) = (unique_ID >> 16) & 0b11111111;  
                   *(*result + buffer_position + 3) = (unique_ID >> 24) & 0b11111111;   
                   *(*result + buffer_position + 4) = (*interactome).unique_ID & 0b11111111;
                   *(*result + buffer_position + 5) = ((*interactome).unique_ID >> 8) & 0b11111111; 
                   *(*result + buffer_position + 6) = ((*interactome).unique_ID >> 16) & 0b11111111;  
                   *(*result + buffer_position + 7) = ((*interactome).unique_ID >> 24) & 0b11111111;   
                   buffer_position += 8;
                   write_index(*result, &buffer_position, task_subindex, true);
                  
                   write_index(*result, &buffer_position, subselection, false);

                   time(&end_time);
                   #if global_time_recording_enabled 
                   double dtime = difftime(end_time, start_time);
                   #else 
                   double dtime = 0;
                   #endif 
                   write_double(*result, &buffer_position, dtime);
                   #if cout_output_enabled 
                   cout << "Query processed in: " <<  dtime << " s\n";
                   #endif
                   for (i = 0; i < subselection; ++i) {
                       write_float(*result, &buffer_position, *(correlations + i)); 
                       save_bytes(*(compressed_selections + i), *(compressed_selection_lengths + i), *result, &buffer_position);
                       if (return_full_vectors){
                           save_bytes(*(compressed_vectors + i*2), *(compressed_vector_lengths + i*2), *result, &buffer_position);
                           save_bytes(*(compressed_vectors + i*2 + 1), *(compressed_vector_lengths + i*2 + 1), *result, &buffer_position); 
                       };
                   };

               byte *sha;
               sha = get_sha256(*result, buffer_position);
               save_bytes(sha, 32, *result, &buffer_position);
               delete [] sha; 

    
                //tidy up
                if (return_full_vectors) {
                    for (i = 0; i < subselection * 2; ++i) {
                        if (*(compressed_vector_lengths + i) > 0) {
                            delete [] *(compressed_vectors + i);
                        };
                    };
                    delete [] compressed_vectors;
                    delete [] compressed_vector_lengths;
                };
            
                for (i = 0; i < subselection; ++i) {
                    if (*(compressed_selection_lengths + i) > 0) {
                        delete [] *(compressed_selections + i);
                    };                
                };
                delete [] compressed_selections;     
                delete [] compressed_selection_lengths; 
                delete [] correlations;

            }
            delete [] combi_selection;
            delete [] gene_set1;
            delete [] gene_set2;

        } else {
               //result_vectors = new *float [count];
               compressed_selections = new byte* [count];
               compressed_selection_lengths = new int [count];
               compressed_vectors = new byte* [count];
               compressed_vector_lengths = new int [count];
               compressed_selection_length = count;
               for (i = 0; i < count; ++i) {               
                    *(compressed_selection_lengths + i) = 0;
                    *(compressed_vector_lengths + i) = 0;
               }; 
               vector_length = 0;

               try { 
                   for (i = 0; i < count; ++i) {
                        set_bool(selection, count, false);
                        *(selection + i) = true;
                        compress_selection(compressed_selections, compressed_selection_lengths, i, selection);
                        vector = accumulate_vector(selection, &vector_length);
                        if (i < subset1) {
                            (*interactome).set_settings(1);
                        }
                        else {
                            (*interactome).set_settings(2);
                        };            
                        (*interactome).propagate(vector, vector_length);
                        compress_vector(compressed_vectors, compressed_vector_lengths, (*interactome).genes, i);
                        if (vector_length > 0) {
                            delete [] vector;
                            vector_length = 0;
                        };
                   };     
                   // Generate results with results vectors here
                   size = 15 + 8 + 32 + 4 + 9; //IDs and combinations count, + 8 bytes for time elapsed. + hash + taskindex + header
                   for (i = 0; i < count; ++i) {
                        size += *(compressed_selection_lengths + i);
                        size += *(compressed_vector_lengths + i);
                   }; 
               }
               catch(...) {
                    #if cout_output_enabled
                   cout <<"Error in processing...\n";
                    #endif

                   if (vector_length > 0) {
                        delete [] vector;
                    };

                   if (compressed_selection_length > 0){
                       for (i = 0; i < count; ++i) {
                            if (*(compressed_vector_lengths + i) > 0) {
                                delete [] *(compressed_vectors + i);
                            };
                            if (*(compressed_selection_lengths + i) > 0) {
                                delete [] *(compressed_selections + i);
                            };
                       };
                       delete [] compressed_vector_lengths;
                       delete [] compressed_selection_lengths; 
                   }; 
                   if (selection_count > 0) {
                        delete [] selection;    
                   };

                   throw "Error in processing";
               };
                
               //Generate result buffer
                   *result = new byte [size];   
                   *result_line_size = size;    
                
                   buffer_position = 0; 
                   *(*result + buffer_position + 0) = 13;
                   *(*result + buffer_position + 1) = 21;
                   *(*result + buffer_position + 2) = 6;
                   *(*result + buffer_position + 3) = 73;
                   *(*result + buffer_position + 4) = 17;
                   *(*result + buffer_position + 5) = 18;
                   *(*result + buffer_position + 6) = 80;
                   *(*result + buffer_position + 7) = 180;
                   *(*result + buffer_position + 8) = 210;
                   buffer_position += 9;

                   *(*result + buffer_position) = query_settings;
                   buffer_position += 1;

                   write_index(*result, &buffer_position, subset1, false);
                   write_index(*result, &buffer_position, subset2, false);

                   *(*result + buffer_position + 0) = unique_ID & 0b11111111;
                   *(*result + buffer_position + 1) = (unique_ID >> 8) & 0b11111111; 
                   *(*result + buffer_position + 2) = (unique_ID >> 16) & 0b11111111;  
                   *(*result + buffer_position + 3) = (unique_ID >> 24) & 0b11111111;   
                   *(*result + buffer_position + 4) = (*interactome).unique_ID & 0b11111111;
                   *(*result + buffer_position + 5) = ((*interactome).unique_ID >> 8) & 0b11111111; 
                   *(*result + buffer_position + 6) = ((*interactome).unique_ID >> 16) & 0b11111111;  
                   *(*result + buffer_position + 7) = ((*interactome).unique_ID >> 24) & 0b11111111;   
                   buffer_position += 8;
                   write_index(*result, &buffer_position, task_subindex, true);
                   write_index(*result, &buffer_position, count, false);

               time(&end_time);
                   #if global_time_recording_enabled 
                   double dtime = difftime(end_time, start_time);
                   #else 
                   double dtime = 0;
                   #endif 
               write_double(*result, &buffer_position, dtime);
                #if cout_output_enabled
               cout << "Query processed in: " <<  dtime << " s\n";
                #endif
               //no correlations, so going on... 
               for (i = 0; i < count; ++i) {
                   save_bytes(*(compressed_selections + i), *(compressed_selection_lengths + i), *result, &buffer_position);
                   save_bytes(*(compressed_vectors + i), *(compressed_vector_lengths + i), *result, &buffer_position);
               };
               
               byte *sha;
               sha = get_sha256(*result, buffer_position);
               save_bytes(sha, 32, *result, &buffer_position);
               delete [] sha; 

               // Tidy up          
               for (i = 0; i < count; ++i) {
                    if (*(compressed_vector_lengths + i) > 0) {
                        delete [] *(compressed_vectors + i);
                    };
                    if (*(compressed_selection_lengths + i) > 0) {
                        delete [] *(compressed_selections + i);
                    };
               };
               delete [] compressed_vector_lengths;
               delete [] compressed_selection_lengths; 
        };

        delete [] selection;    

        //returning result
        
    };

};



void load_sets(char* fname_A, char* fname_B, byte*** result, int* buff_A_size, int** buff_A_sizes) {

    streampos size;
    char * memblock_A;
    int memsize_A = 0;
    char * memblock_B;
    int memsize_B = 0;
    char * memblock;
    int memsize = 0;

    int i;
    int j;

    ifstream file_A (fname_A, ios::in|ios::binary|ios::ate);
    if (file_A.is_open()) {
        try{
            size = file_A.tellg();
            memblock_A = new char [size];
            memsize_A = size;
            file_A.seekg (0, ios::beg);
            file_A.read (memblock_A, size);
            file_A.close();
            #if cout_output_enabled    
            cout << "the entire file A content is in memory\n";
            cout << size << " bytes loaded\n";
            #endif
        }
        catch (...) {
            #if cout_output_enabled    
            cout <<"Error reading file A...\n";
            #endif    
            if (memsize_A > 0) {
                delete [] memblock_A;
            }
            throw "Error reading A";
        };
    } else {
        #if cout_output_enabled    
        cout <<"Error! Could not open file with set A! \n";
        #endif
        throw "Could not open file A";
    };

    ifstream file_B (fname_B, ios::in|ios::binary|ios::ate);
    if (file_B.is_open()) {
        try{
            size = file_B.tellg();
            memblock_B = new char [size];
            memsize_B = size;
            file_B.seekg (0, ios::beg);
            file_B.read (memblock_B, size);
            file_B.close();
            #if cout_output_enabled    
            cout << "the entire file B content is in memory\n";
            cout << size << " bytes loaded\n";
            #endif
        }
        catch (...) {
            #if cout_output_enabled    
            cout <<"Error reading file B...\n";
            #endif    
            if (memsize_A > 0) {
                delete [] memblock_A;
            }
            if (memsize_B > 0) {
                delete [] memblock_B;
            }
            throw "Error reading B";
        };
    } else {
        #if cout_output_enabled    
        cout <<"Error! Could not open file with set B! \n";
        #endif
        if (memsize_A > 0) {
            delete [] memblock_A;
        }
        throw "Could not open file B";
    };

    memsize = memsize_A + memsize_B + 1;
    memblock = new char [memsize];
    char_copy(memblock_A, memblock, memsize_A);
    *(memblock + memsize_A) = 13;
    char_copy(memblock_B, memblock + memsize_A + 1, memsize_B);

    //Parse all lines
            bool after_newline = true;

            *buff_A_size = 0; 
            for (i = 0; i < memsize; ++i) {
                if ((int(*(memblock + i)) != 13) && (int(*(memblock + i)) != 10)) {
                    if (after_newline) {
                        *buff_A_size += 1;
                        after_newline = false;
                    };
                } else {
                    after_newline = true;
                };
            };
        
            *result = new byte* [*buff_A_size];
            *buff_A_sizes = new int [*buff_A_size];

            for (i = 0; i < *buff_A_size; ++i) {
                *(*buff_A_sizes + i) = 0;
            };
        
            int subcount = 0;
            int subindex = -1;

            after_newline = true;     
            bool in_line = false;

            for (i = 0; i < memsize; ++i) {
                if ((int(*(memblock + i)) != 13) && (int(*(memblock + i)) != 10)) {
                    in_line = true;
                    if (after_newline) {
                        subindex += 1;
                        subcount = 0;
                        after_newline = false;
                    };
                    subcount += 1;
                } else {
                    if (in_line) {
                        *((*buff_A_sizes) + subindex) = subcount;
                        (*(*result + subindex)) = new byte [subcount];
                        subcount = 0;
                    };
                    in_line = false;
                    after_newline = true;
                };
            };

            if (in_line) {
                *((*buff_A_sizes) + subindex) = subcount;
                (*(*result + subindex)) = new byte [subcount];
                subcount = 0;
            };
            
            #if cout_output_enabled
            cout << subindex + 1 << "non-empty lines found:\n";
            #endif
            //throw;
            subcount = 0;
            subindex = -1;

            after_newline = true;     
            in_line = false;


            for (i = 0; i < memsize; ++i) {
                if ((int(*(memblock + i)) != 13) && (int(*(memblock + i)) != 10)) {
                    in_line = true;
                    if (after_newline) {
                        subindex += 1;
                        subcount = 0;
                        after_newline = false;
                    };
                    *(*(*result + subindex) + subcount) = int(*(memblock + i));
                    //cout << *(*(*result + subindex) + subcount);
                    subcount += 1;
                } else {
                    if (in_line) {
                        #if cout_output_enabled
                        cout << "\n";
                        #endif
                    };
                    in_line = false;
                    after_newline = true;
                };
            };




    if (memsize > 0) {
        delete [] memblock;
    }

    if (memsize_B > 0) {
        delete [] memblock_B;
    }

    if (memsize_A > 0) {
        delete [] memblock_A;
    }


};




static void store_results_dataset(byte** results_buff, int* results_sizes, int results_size, char* fname) {
    int i;
    int j;
    ofstream outfile (fname);
    if (outfile.is_open()){
        for (i = 0; i < results_size; ++i) {
            for (j = 0; j < *(results_sizes + i); ++j) {
                outfile << char(*(*(results_buff + i) + j));
            };      
            outfile << "\n";
        };
    outfile.close();
    };
};




byte base64encoderrecord[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

byte charconverter_forward[256];
byte charconverter_reverse[256];



void init_decoder() {
    int i;
    for (i = 0; i < 256; ++i) {
        charconverter_reverse[i] = 255;
    }
    for (i = 0; i < 65; ++i) {
        charconverter_forward[i] = int(*(base64encoderrecord + i));
        charconverter_reverse[int(*(base64encoderrecord + i))] = i;
    };
};

static void decode_quad(byte* out_buf, byte b[4]) {
    *(out_buf   )  =  (b[0]               << 2) | ((b[1] & 0b00110000) >> 4);
    *(out_buf + 1) = ((b[1] & 0b00001111) << 4) | ((b[2] & 0b00111100) >> 2);
    *(out_buf + 2) = ((b[2] & 0b00000011) << 6) | ((b[3] & 0b00111111)    );
};

static void encode_triplet(byte* out_buf, byte b[3]) {

    *(out_buf   )  =  (b[0] & 0b11111100) >> 2;
    *(out_buf + 1) = ((b[0] & 0b00000011) << 4) | ((b[1] & 0b11110000) >> 4);
    *(out_buf + 2) = ((b[1] & 0b00001111) << 2) | ((b[2] & 0b11000000) >> 6);
    *(out_buf + 3) = ((b[2] & 0b00111111));
};


void decode_from_base64(byte* buffer, int buffer_size, byte** output_buffer, int* return_buffer_size) {
    if (buffer_size % 4 != 0) {
        #if cout_output_enabled
        cout <<"Error! buffer size not divisible by 4!\n";
        #endif
        throw "Buffer not divisible by 4";
    };

    
    int size = buffer_size / 4 * 3;

    *output_buffer = new byte [size];
    

    byte b[4];
    
    int i;
    for (i = 0; i < buffer_size/4; ++i) {
        b[0] = charconverter_reverse[int(*(buffer + i*4    ))];
        b[1] = charconverter_reverse[int(*(buffer + i*4 + 1))];
        b[2] = charconverter_reverse[int(*(buffer + i*4 + 2))];
        b[3] = charconverter_reverse[int(*(buffer + i*4 + 3))];
        if ((b[0] == 255) || (b[1] == 255) || (b[2] == 255) || (b[3] == 255)) {
            #if cout_output_enabled
            cout <<"Wrong character in encoded string!\n";
            #endif
            throw "Wrong character!";
        };

        decode_quad((*output_buffer + i*3), b);
    };


    if (*(buffer + buffer_size - 1) == '=') {
        size -= 1;
    };
    if (*(buffer + buffer_size - 2) == '=') {
        size -= 1;
    };

    *return_buffer_size = size;

};


void encode_to_base64(byte* buffer, int buffer_size, byte** output_buffer, int* return_buffer_size) {
    
    int triplets = buffer_size / 3;
    int tail = buffer_size % 3;
    int size = triplets * 4;
    if (tail != 0) {
        size += 4;
    }

    *output_buffer = new byte [size];
    *return_buffer_size = size;

    byte b[3];
    
    int i;
    for (i = 0; i < buffer_size / 3; ++i) {
        b[0] = *(buffer + i*3    );
        b[1] = *(buffer + i*3 + 1);
        b[2] = *(buffer + i*3 + 2);

        encode_triplet((*output_buffer + i*4), b);

        *(*output_buffer + i*4) = charconverter_forward[int(*(*output_buffer + i*4))];
        *(*output_buffer + i*4 + 1) = charconverter_forward[int(*(*output_buffer + i*4 + 1))];
        *(*output_buffer + i*4 + 2) = charconverter_forward[int(*(*output_buffer + i*4 + 2))];
        *(*output_buffer + i*4 + 3) = charconverter_forward[int(*(*output_buffer + i*4 + 3))];
    };

    if (tail == 1) {
        b[0] = *(buffer + buffer_size - 1);
        b[1] = 0;
        b[2] = 0;
        encode_triplet((*output_buffer + buffer_size / 3 * 4), b);
        *(*output_buffer + size - 2) = '=';
        *(*output_buffer + size - 1) = '=';
        *(*output_buffer + size - 3) = charconverter_forward[int(*(*output_buffer + size - 3))];
        *(*output_buffer + size - 4) = charconverter_forward[int(*(*output_buffer + size - 4))];

    } else if (tail == 2) {
        b[0] = *(buffer + buffer_size - 2);
        b[1] = *(buffer + buffer_size - 1);
        b[2] = 0;
        encode_triplet((*output_buffer + buffer_size / 3 * 4), b);
        *(*output_buffer + size - 1) = '=';
        *(*output_buffer + size - 2) = charconverter_forward[int(*(*output_buffer + size - 2))];
        *(*output_buffer + size - 3) = charconverter_forward[int(*(*output_buffer + size - 3))];
        *(*output_buffer + size - 4) = charconverter_forward[int(*(*output_buffer + size - 4))];
    };
};





//Main processig function
int process_datasets(char* set_A, char* set_B, char* out_file) {
    init_decoder();

    byte **buff_A;
    int *buff_A_sizes;
    int buff_A_size = 0;
    
    byte *data_type;
    byte **data_buff;
    int *data_len;
    int data_size = 0;

    byte **results_buff;
    int *results_sizes;
    int results_size = 0;
    int i;
    int j;

    
    byte *decoded_result_line;
    int result_line_size = 0;

    byte *encoded_result_line;
    int encoded_result_line_size = 0;
    
        
    try {
       
        load_sets(set_A, set_B, &buff_A, &buff_A_size, &buff_A_sizes);
        data_size = buff_A_size;
        data_type = new byte [data_size];
        data_buff = new byte* [data_size];
        data_len = new int [data_size];
        for (i = 0; i < data_size; ++i) {
            *(data_len + i) = 0;
            *(data_type + i) = 0;
        };
        
        for (i = 0; i < buff_A_size; ++i) {
            #if cout_output_enabled
                cout <<"Processing line " << i + 1 << " of " << buff_A_size << "...\n";
            #endif    
            byte *decoded_line_A;
            int line_A_size = 0;
            
            try {
                #if cout_output_enabled
                    cout <<"Base64 decoding...\n";
                #endif    

                decode_from_base64(*(buff_A + i), *(buff_A_sizes + i), &decoded_line_A, &line_A_size);

                #if cout_output_enabled
                    cout <<"SHA256 verification ...\n";
                #endif
                
                if (line_A_size < 32) {
                    #if cout_output_enabled
                        cout <<"No hash at the end of the line!\n";
                    #endif
                    throw "No hash found!";
                };

                verify_hash(decoded_line_A, line_A_size - 32, decoded_line_A + line_A_size - 32);
                #if cout_output_enabled
                    cout <<"SHA256 verification passed!\n";
                #endif
                
                #if cout_output_enabled
                    cout <<"Type:\n";
                #endif
                
                if (line_A_size < 50) {
                    #if cout_output_enabled
                        cout <<"Incomplete file!\n";
                    #endif
                    throw "Incomplete file!";
                };
                
                if ((*(decoded_line_A + 0) == 32) &&
                    (*(decoded_line_A + 1) == 212) &&
                    (*(decoded_line_A + 2) == 196) &&
                    (*(decoded_line_A + 3) == 68) &&
                    (*(decoded_line_A + 4) == 0) &&
                    (*(decoded_line_A + 5) == 147)) { //This is a dataset - starts with INTERACT
                    *(data_type + i) = 1; //1 for Dataset
                    *(data_buff + i) = decoded_line_A;
                    *(data_len + i) = line_A_size;
                    line_A_size = 0;
                    #if cout_output_enabled
                        cout <<"Dataset found!\n";
                    #endif
                    
                }
                else
                if ((*(decoded_line_A + 0) == 13) &&
                    (*(decoded_line_A + 1) == 21) &&
                    (*(decoded_line_A + 2) == 6) &&
                    (*(decoded_line_A + 3) == 73) &&
                    (*(decoded_line_A + 4) == 5) &&
                    (*(decoded_line_A + 5) == 4) &&
                    (*(decoded_line_A + 6) == 68) &&
                    (*(decoded_line_A + 7) == 129) &&
                    (*(decoded_line_A + 8) == 18)) { //This is a task - starts with DRUGSQUERIES
                    *(data_type + i) = 2; //2 for Task
                    *(data_buff + i) = decoded_line_A;
                    *(data_len + i) = line_A_size;
                    line_A_size = 0;
                    #if cout_output_enabled
                        cout <<"Task found!\n";
                    #endif
                    
                } else {
                    #if cout_output_enabled
                        cout <<"Unrecognized data!\n";
                    #endif
                };

            } catch (...) {
                #if cout_output_enabled
                    cout <<"Line " << i + 1 << " of " << buff_A_size << " failed!\n";
                #endif    
            };

            if (line_A_size > 0) {
                delete [] decoded_line_A;
                line_A_size = 0;
            };
        };
        
        
        //Count datasets and tasks
        int task_count = 0;
        int dataset_count = 0;
        for (i = 0; i < data_size; ++i) {
            if (*(data_type + i) == 1) {
                dataset_count += 1;
            } else
            if (*(data_type + i) == 2) {
                task_count += 1;
            };
        };

        #if cout_output_enabled
            cout <<"N datasets: " << dataset_count << ", N tasks: " << task_count << "\n";
        #endif    

        
        if ((dataset_count > 0) && (task_count > 0)) {
            results_size = dataset_count * task_count;
            results_buff = new byte* [results_size];
            results_sizes = new int [results_size];
            for (i = 0; i < results_size; ++i) {
                *(results_sizes + i) = 0;
            };

        int result_index = 0;
            
        for (i = 0; i < data_size; ++i) {
            if (*(data_type + i) == 1) { // i - dataset
                #if cout_output_enabled
                    cout <<"Dataset Index: " << i <<"\n";
                #endif    
        
                Interactome interactome(*(data_buff + i), *(data_len + i) - 32);


                for (j = 0; j < data_size; ++j) {
                    if (*(data_type + j) == 2) { // j - task
                        #if cout_output_enabled
                            cout << "Task Index: " << j << "\n";
                        #endif    
                        QuerySet query_set(&interactome, *(data_buff + j), *(data_len + j) - 32);
                        query_set.process_query(&decoded_result_line, &result_line_size);                    
                        encode_to_base64(decoded_result_line, result_line_size, &encoded_result_line, &encoded_result_line_size);
                        if (result_line_size > 0) {
                            delete [] decoded_result_line;
                            result_line_size = 0;
                        };
                        *(results_buff + result_index) = encoded_result_line;
                        *(results_sizes + result_index) = encoded_result_line_size;
                        encoded_result_line_size = 0;
                
                        #if cout_output_enabled
                            cout <<"Result Index: " << result_index << "\n";
                        #endif    
                        result_index += 1;
                    };
                };
            };
        };

        store_results_dataset(results_buff, results_sizes, results_size, out_file);
        } else {
            #if cout_output_enabled
                cout <<"No valid combinations found!\n";
            #endif    
        };

    }
    catch (...){
        #if cout_output_enabled    
        cout <<"Error occured! Terminating and tidying up...\n";
        #endif

        #if global_error_throw_enabled
        if (results_size > 0) {
            for (i = 0; i < results_size; ++i) {
                if (*(results_sizes + i) > 0) {
                    delete [] (*(results_buff + i));
                };
            };
            delete [] results_sizes;
            delete [] results_buff;
        };    
            
        if (buff_A_size > 0)  {
        //Free set A memory
            for (i = 0; i < buff_A_size; ++i) {
                if (*(buff_A_sizes + i) > 0) {
                    delete [] (*(buff_A + i));
                };
            };
            delete [] buff_A_sizes;
            delete [] buff_A;
        };
    
        if (data_size > 0)  {
        //Free set A memory
            for (i = 0; i < data_size; ++i) {
                if (*(data_len + i) > 0) {
                    delete [] (*(data_buff + i));
                };
            };
            delete [] data_len;
            delete [] data_buff;
            delete [] data_type;
        };
        if (encoded_result_line_size > 0) {
            delete [] encoded_result_line;
        };
    
        if (result_line_size > 0) {
            delete [] decoded_result_line;
        };

        throw "Error occured! Terminating and tidying up";
        #endif

    };

    
    if (encoded_result_line_size > 0) {
        delete [] encoded_result_line;
    };

    if (result_line_size > 0) {
        delete [] decoded_result_line;
    };

    //Free results memory
    if (results_size > 0) {
        for (i = 0; i < results_size; ++i) {
            if (*(results_sizes + i) > 0) {
                delete [] (*(results_buff + i));
            };
        };
        delete [] results_sizes;
        delete [] results_buff;
    };    
        
    if (buff_A_size > 0)  {
    //Free set A memory
        for (i = 0; i < buff_A_size; ++i) {
            if (*(buff_A_sizes + i) > 0) {
                delete [] (*(buff_A + i));
            };
        };
        delete [] buff_A_sizes;
        delete [] buff_A;
    };

    if (data_size > 0)  {
    //Free set A memory
        for (i = 0; i < data_size; ++i) {
            if (*(data_len + i) > 0) {
                delete [] (*(data_buff + i));
            };
        };
        delete [] data_len;
        delete [] data_buff;
        delete [] data_type;
    };

    return 0;
};


