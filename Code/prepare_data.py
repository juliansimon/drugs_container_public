# -*- coding: utf-8 -*-
"""
DRUGS Project module: prepare_data.py

Created on Mon Jan 15 11:52:26 2018

This module prepares input files for the propagation algorithm according to 
settings in settings.ini and provided test and training data in ../Compounds.

It should be executed first by running: 

python prepare_data.py

It will produce intermediate files in ../SCRATCH and log in ../RESULTS by default.

Once successfully run, it should be followed by the run_propagation.py module 
to generate propagated gene profiles for subsequent analysis.

This stage is platform-independent and can be done using both python 2 and 3. 
Anaconda Python 3.x 64bit is recommended with Pandas and NumPy pre-installed.


--------------------------------------------------------------------------

Lead Developer: Dr. Ivan Laponogov (mailto:i.laponogov@imperial.ac.uk) 

Chief project investigator: Dr. Kirill Veselkov (mailto:kirill.veselkov04@imperial.ac.uk)

License: 

The MIT License (MIT)

Copyright 2019 Imperial College London

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

#Loading global modules and dependencies
import pandas as pd;
import numpy as np;
import os;
import sys;
import datetime;
import hashlib;

#Checking if the system is right and if the module is run as a script instead of being imported
if __name__== '__main__':
    if sys.byteorder!='little':
        print('Only little endian machines currently supported! bye bye ....');
        quit();
else:
    print('This module is not meant to be imported, but should be run as a script instead!')
    quit();
    
#Figuring out and adding the path to local modules    
module_path = os.path.dirname(os.path.realpath(__file__));
sys.path.append(module_path);

#Loading local modules
from printlog import start_log, stop_log, printlog;  #print and log printed messages in one go
from base64_encoder import encode_to_base64;  #corrected base64 encoder
#different functions used by the algorithm
from utils import write_num, \
                  write_float, \
                  load_global_settings,\
                  join_and_map_compounds; 

######################################################################################
#Initialization of the settings, paths and parameters, loading of data files
######################################################################################
settings_file = os.path.abspath(os.path.join(module_path, '../settings.ini'));
root_folder = os.path.dirname(settings_file);
global_settings = load_global_settings(settings_file);

interactome_path = global_settings['Default Paths']['Interactome_Path'];
compounds_path = global_settings['Default Paths']['Compounds_Path'];
scratch_path = global_settings['Default Paths']['Scratch_Path'];
results_path = global_settings['Default Paths']['Results_Path'];

if not os.path.exists(results_path):
    os.makedirs(results_path);
    
if not os.path.exists(scratch_path):
    os.makedirs(scratch_path);

logfile = os.path.join(global_settings['Default Paths']['Results_Path'], 'data_preparation.log');
start_log(logfile, autoclose = True, overwrite_existing = True, verbosity_level = 0);

#####################################################################################
#Loading main data
#####################################################################################

#Genes first
fname = os.path.join(interactome_path, 'genes.csv.gz');
printlog('Genes source file:');
printlog(fname);
printlog('Reading genes...');
genes = pd.read_csv(fname, index_col = 0);
gene_count = len(genes.index);
printlog('%s genes loaded.'%gene_count);

#Gene connections
fname = os.path.join(interactome_path, 'gene_connections.csv.gz');
printlog('Gene connections source file:');
printlog(fname);
printlog('Reading gene connections...');
gene_connections = pd.read_csv(fname, index_col = 0);
gene_connections_count = len(gene_connections.index);
printlog('%s gene connections loaded.'%gene_connections_count);

#Drugs/Compounds
fname = os.path.join(compounds_path, 'compounds.csv.gz');
printlog('Loading compounds from:');
printlog(fname);
compounds_all = pd.read_csv(fname, index_col = 0);
printlog('%s compounds loaded.'%len(compounds_all.index));

labels = compounds_all['Label'].astype(str);

compounds_1 = compounds_all.loc[labels == '1'];
compounds_0 = compounds_all.loc[labels == '0'];
compounds_test = compounds_all.loc[(labels != '1') & (labels != '0')];

printlog('%s positive class 1 compounds loaded.'%len(compounds_1.index));
printlog('%s negative class 0 compounds loaded.'%len(compounds_0.index));
printlog('%s test class "?" compounds loaded.'%len(compounds_test.index));

stitch_selection =  global_settings['Preparation']['STITCH_ID_selection'].lower();

stitch_file = os.path.join(interactome_path, 'mapped.csv.gz');

total_compounds = join_and_map_compounds(compounds_1, 
                                         compounds_0, 
                                         compounds_test, 
                                         stitch_file, 
                                         stitch_selection,
                                         global_settings.getint('Network Propagation Settings', 'gene_compound_threshold'),
                                         global_settings['Default Paths']['Results_Path'],
                                         global_settings['Preparation']['InChIKey_matching'].lower());

############################################################################
#The following section is preparing the dataset with interactome for gene 
#perturbation propagation. The section currently is limited to PageRank method
#with the single set of parameters only, but is adopted from a more general 
#code which allows multiple methods and settings to be tweaked. Thus, a number
#of fields here are redundant for the purpose of this particular section.
#The full range of settings and capabilities of the designed code will be 
#described in the documentation at 
#https://bitbucket.org/iAnalytica/drugs/src/master/
#when ready.
##############################################################################

STRING_threshold = global_settings.getint('Network Propagation Settings', 'STRING_threshold');
BioPlex_confidence_threshold = global_settings.getint('Network Propagation Settings', 'BioPlex_threshold');
BioPlex_threshold = -1.0;

matrix_scale = 1.0;

if not os.path.exists(scratch_path):
    os.makedirs(scratch_path);

normalize_input_matrix = True;
normalize_incoming_connections = False;    
normalize_to_sum = True;
large_address_size =  False;    
    
timestamp = datetime.datetime.now().strftime("%d_%m_%Y_%H_%M_%S");

method1 = 0; #0 for PageRank
method2 = 0;

iterations_limit1 = 300;
iterations_limit2 = 300;
eps1 = 1.0e-6;	
eps2 = 1.0e-6;	
zero_eps1 = 1.0e-9;
zero_eps2 = 1.0e-9;
    
c1 = global_settings.getfloat('Network Propagation Settings', 'PageRank_c');
c2 = c1;

normalize1 = True;
normalize2 = True;
    
post_normalize1 = True;
post_normalize2 = True;

m1 = STRING_threshold <= np.array(gene_connections['STRING']);
m2 = BioPlex_confidence_threshold <= np.array(gene_connections['BioPlex_Confidence']);
mask = np.logical_and(m1, m2);

ids1 = np.array(gene_connections['Gene1'], np.int32)[mask];
ids2 = np.array(gene_connections['Gene2'], np.int32)[mask];

printlog('Dataset will contain %s directional connections out of %s possible...'%(len(ids1), len(gene_connections['Gene1'])));

printlog('Creating connection indeces...')
connects = {};
for j in range(len(ids1)):
    id1 = ids1[j];
    id2 = ids2[j];
    if id1 in connects:
        connects[id1][id2] = 1.0;
    else:
        connects[id1] = {id2:1.0};
    
printlog('Condensing bi-directional connections...')
cc = 0;
connects2 = {};
while connects:
    bestid = '';
    bestcount = 0;
    bestdict = {};
    for id1 in connects.keys():
        count = len(connects[id1]);
        if count > bestcount:
            bestcount = count;
            bestid = id1;
            bestdict = connects[id1];
    if bestcount > 0:
        connects2[bestid] = bestdict;
        del connects[bestid];
        for id2 in bestdict.keys():
            val1 = bestdict[id2];
            bi_directional = False;
            v1 = 1.0; #set to val1 or whatever....
            if id2 in connects:
                dd2 = connects[id2];
                if bestid in dd2:
                    val2 = dd2[bestid];
                    if val2 == val1:
                        bi_directional = True;
                        del dd2[bestid];
                if len(dd2) == 0:
                    del connects[id2];
            newval = 0;
            if bi_directional:
                newval += 1;
            if v1 < 0 :
                newval += 2; #Sign flag 1 = negative
            v1 = abs(v1)/matrix_scale;
            v1 = min(int(v1 * 31), 31);
            newval += v1 * 8;
            bestdict[id2] = newval;
            cc += 1;
    if len(connects) % 1000 == 0:
        print(len(connects))                
    
printlog('Condensing completed...');
printlog('New number of connections: %s'%cc);    

weights = np.full((gene_count,), 1.0, dtype = np.float32);

printlog('Generating output binary stream...');
out = bytearray([32, 212, 196, 68, 0, 147]);            

out.append(0);
out.append(0);
out.append(0);
out.append(0);

#Writing settings
settings_byte = 0;
    
if large_address_size:
    settings_byte += 1; #bit 0
        
if normalize_input_matrix: #Pre_normalization of the matrix prior to multiplication
    settings_byte += 4; #bit 2
        
if normalize_incoming_connections: #Outgoing if False, incoming if True
    settings_byte += 8; #bit 3    
        
if normalize_to_sum:
    settings_byte += 16; #bit 4
    
out.append(settings_byte);
out.append(method1 & 0b11111111);
out.append(method2 & 0b11111111);

#Write number of genes
write_num(out, gene_count, large_address_size);
    
#write iterations_limit
write_num(out, iterations_limit1, False);
write_num(out, iterations_limit2, False);
    
write_float(out, eps1);
write_float(out, eps2);
write_float(out, zero_eps1);
write_float(out, zero_eps2);
    
#write c parameter
write_float(out, c1);
#normalization settings
v = 0;
if normalize1:
    v += 1;
if post_normalize1:
    v += 2;
out.append(v);
        
#write c parameter
write_float(out, c2);

#normalization settings
v = 0;
if normalize2:
    v += 1;
if post_normalize2:
    v += 2;
out.append(v);

for j in range(gene_count):
    write_float(out, weights[j]);
    
#write global scaling factor
write_float(out, matrix_scale);
printlog('N: entries: %s'%len(connects2.keys()));
#write sparse matrix
for key in connects2.keys():
    id1 = key;
    dd = connects2[key];
    count = len(dd);
    write_num(out, id1, large_address_size);
    write_num(out, count, large_address_size);
    for key2 in dd.keys():
        id2 = key2;
        score = dd[key2];
        write_num(out, id2, large_address_size);
        out.append(score & 0b11111111);
            
printlog("Final buffer length: %s"%len(out))

printlog('Calculating sha256 hash...');
hh = bytearray(hashlib.sha256(out).digest());
out.extend(hh);
#Converting to base64
printlog('Text output generation...')
text_out = encode_to_base64(out);
printlog('Test length: %s'%len(text_out));
with open(os.path.join(scratch_path, 'base_dataset.txt'), 'w') as fout:
    fout.write('%s\n'%text_out);

    printlog('Storing metadata in external file....');
    
with open(os.path.join(scratch_path, 'base_dataset.meta'), 'w') as fout:
    fout.write('Gene count: %s\n'%gene_count);
    fout.write('STRING_threshold: %s\n'%STRING_threshold);
    fout.write('BioPlex_confidence_threshold: %s\n'%BioPlex_confidence_threshold);
    fout.write('matrix_scale: %s\n'%matrix_scale);
    fout.write('outpath: %s\n'%scratch_path);
    fout.write('normalize_input_matrix: %s\n'%normalize_input_matrix);
    fout.write('normalize_incoming_connections: %s\n'%normalize_incoming_connections);
    fout.write('normalize_to_sum: %s\n'%normalize_to_sum);
    fout.write('large_address_size: %s\n'%large_address_size);
    fout.write('timestamp: %s\n'%timestamp);
    fout.write('method1: %s\n'%method1);
    fout.write('method2: %s\n'%method2);
    fout.write('iterations_limit1: %s\n'%iterations_limit1);
    fout.write('iterations_limit2: %s\n'%iterations_limit2);
    fout.write('eps1: %s\n'%eps1);
    fout.write('eps2: %s\n'%eps2);
    fout.write('zero_eps1: %s\n'%zero_eps1);
    fout.write('zero_eps2: %s\n'%zero_eps2);
    fout.write('c1: %s\n'%c1);
    fout.write('c2: %s\n'%c2);
    fout.write('normalize1: %s\n'%normalize1);
    fout.write('normalize2: %s\n'%normalize2);
    fout.write('post_normalize1: %s\n'%post_normalize1);
    fout.write('post_normalize2: %s\n'%post_normalize2);

##############################################################################
#The following section prepares the task file with compound-gene connections
#for propagation. It is adopted from a more general code which allows multiple 
#methods and settings to be tweaked as well as correlation between combinations 
#of compounds calculated. Thus, a number of fields here are redundant for the 
#purpose of this particular section. The full range of settings and capabilities 
#of the designed code will be described in the documentation at 
#https://bitbucket.org/iAnalytica/drugs/src/master/
#when ready.
##############################################################################

printlog('Preparing task...');

return_cross_correlations = False;
return_all_combinations = False;
return_full_vectors = True;
return_byte_scaled = False;
batch2_maxn_combinations = 1;

printlog('Generating output binary stream...');
out = bytearray([13,21,6,73,5,4,68,129,18]);            
out.append(0);
out.append(0);
out.append(0);
out.append(0);
out.append(0);
out.append(0);
out.append(0);
out.append(0);
out.append(0);
out.append(0);
out.append(0);
out.append(0);
    
formatbyte = 0;
if large_address_size:
    formatbyte += 1;
if return_full_vectors:
    formatbyte += 2;
if return_cross_correlations:
    formatbyte += 4;
if return_all_combinations:
    formatbyte += 8;
if return_byte_scaled:
    formatbyte += 16;    
formatbyte += (((batch2_maxn_combinations - 1) & 0b111) << 5);
out.append(formatbyte);
                
subs = total_compounds['Genes'];
subs_i = total_compounds['Indeces'];
                
SetA_selection_conversion = encode_to_base64(np.array(subs_i, dtype = np.int32));
                
if SetA_selection_conversion == '' and len(subs_i) == 1:
    SetA_selection_conversion = 'AAAAAA==';
        
write_num(out, len(subs), l_address_size = False);
write_num(out, 0, l_address_size = False);
            
for item_raw in subs:
    gene_ids = item_raw;
    item = np.zeros((gene_count, ), dtype = np.float32);
    for j in gene_ids:
        item[j] = 1.0;
                    
    mask = item != 0.0;
    m_count = np.sum(mask);
    printlog('N mutations/interactions: %s'%m_count);
    vals = item[mask];
    indcs = np.arange(item.shape[0])[mask];
    unique = np.unique(vals);
    can_use_bits = False;
    if len(unique) == 1:
        if unique[0] == 1.0:
            can_use_bits = True;
                
    buf_sparse = bytearray();
    buf_full = bytearray();
                    
    #try both sparse and full formats
                    
    #if only 0 and 1 used - compress into bit representation    
    if can_use_bits:
        bitform = np.packbits(item.astype(np.uint8));
        for b in bitform:
            buf_full.append(b);
    else:
        for i in item:
            write_float(buf_full, i);
                            
    #now try sparse format
    for i in range(m_count):
        val = vals[i];
        indc = indcs[i];
        if val != 1.0:
            #use float
            write_num(buf_sparse, -indc, large_address_size);
            write_float(buf_sparse, val);
        else:
            write_num(buf_sparse, indc, large_address_size);
                    
    if len(buf_sparse) < len(buf_full):
        write_num(out, m_count, large_address_size);
        out += buf_sparse;
    else:
        if can_use_bits:
            write_num(out, -2, large_address_size);
        else:
            write_num(out, -1, large_address_size);
        out += buf_full;
                    
#Converting to base64
printlog('Text output generation...')
    
printlog('Calculating sha256 hash...');
hh = bytearray(hashlib.sha256(out).digest());
out.extend(hh);
    
with open(os.path.join(scratch_path, 'task.txt'), 'w') as fout:
    fout.write('%s\n'%encode_to_base64(out));
    
with open(os.path.join(scratch_path, 'ids.csv'), 'w') as fout:
    fout.write('Index,Group,ID,Name,InChIKey\n');
    for j in range(len(subs)):
        fout.write('%s,"%s","%s","%s","%s"\n'%(j, 
                                          total_compounds['Group'][j], 
                                          total_compounds['IDs'][j], 
                                          total_compounds['Name'][j], 
                                          total_compounds['InChIKey'][j]));
                
printlog('Storing metadata in external file....');
        
with open(os.path.join(scratch_path, 'task.meta'), 'w') as fout:
    fout.write('Gene count: %s\n'%gene_count);
    fout.write('outpath: %s\n'%scratch_path);
    fout.write('large_address_size: %s\n'%large_address_size);
    fout.write('timestamp: %s\n'%timestamp);
    fout.write('connection_threshold: %s\n'%global_settings.getint('Network Propagation Settings', 'gene_compound_threshold'));
    fout.write('SetA_subcount: %s\n'%len(subs));
    fout.write('Return_vectors: %s\n'%return_full_vectors);
    

printlog('Data preparation finished. Run propagation next!')

stop_log();



