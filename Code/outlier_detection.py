# -*- coding: utf-8 -*-
"""
DRUGS Project module: outlier_detection.py
    
Created on Mon Nov 05 15:23:23 2018

Simple implementation of the outlier detection filter using
Mahalanobis distance and Elliptic envelope methods.

--------------------------------------------------------------------------

Lead Developer: Dr. Ivan Laponogov (mailto:i.laponogov@imperial.ac.uk) 

Chief project investigator: Dr. Kirill Veselkov (mailto:kirill.veselkov04@imperial.ac.uk)

License: 

The MIT License (MIT)

Copyright 2019 Imperial College London

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

"""
import numpy as np;
from sklearn.preprocessing import StandardScaler;
from sklearn.covariance import EllipticEnvelope;
from sklearn.decomposition import PCA;
from scipy.stats.distributions import chi2

class OutlierDetector:
    def __init__(self, PCA_threshold = 0.0, contamination = 0.1, cut_off = 0.99):
        self.PCA_threshold_ = PCA_threshold;
        self.contamination_ = contamination;
        self.cut_off_ = cut_off;
    
    def fit(self, X):
        self.scaler_ = StandardScaler();
        X = self.scaler_.fit_transform(X);
        self.PCA_ = PCA();
        
        if self.PCA_threshold_ != 0.0:
            pca  = PCA();
            pca.fit(X);
            
            exp_var = 1.0 - self.PCA_threshold_;
            cum_sum = np.cumsum(pca.explained_variance_ratio_);
            n_components = np.sum(cum_sum <= exp_var);
            self.pca_ = PCA(n_components = n_components);
            
        else:
            self.pca_ = PCA();

        X = self.pca_.fit_transform(X);

        self.estimator_ = EllipticEnvelope(contamination = self.contamination_);
        self.estimator_.fit(X);
        
        return self
        
    def predict(self, X):
        X = self.scaler_.transform(X);
        self.X_ = self.pca_.transform(X);
        z = np.sqrt(self.estimator_.mahalanobis(self.X_));
        thresh = np.sqrt(chi2.ppf(self.cut_off_, df = self.pca_.n_components_)); 
        return z < thresh #Return boolean mask for normal measures (True) and outliers (False)
        
    def mahalanobis(self, X):
        X = self.scaler_.transform(X);
        X = self.pca_.transform(X);
        return self.estimator_.mahalanobis(X);
        
         

    
