# -*- coding: utf-8 -*-
"""
DRUGS Project module: run_machine_learning.py

Created on Sat Dec  8 18:42:07 2018

This module performs machine learning using propagated profiles to predict
the probability of the compounds in test set to be of class 1 as opposite to
belonging to class 0. This is the last module which needs to be run in the sequence
of 3 modules to perform the prediction of the anti-cancer likeness of the
compounds both in test and training sets. It requires Scikit-learn and matplotlib
in addition to NumPy, SciPy and Pandas in your python environment. It should
work normally both in Python versions 2 & 3 and be platform-independent. 

To run it simply use:

python run_machine_learning.py

Note! If you use outlier detection the memory consumption can jump
up to 12-16 Gb.     

The machine learning algorithms are tuned to work with imbalanced classes
and use StratifiedKfold and balanced targets for all the methods. Still, 
for good performance try to keep the smaller class at least 10% of the larger
one.


--------------------------------------------------------------------------

Lead Developer: Dr. Ivan Laponogov (mailto:i.laponogov@imperial.ac.uk) 

Chief project investigator: Dr. Kirill Veselkov (mailto:kirill.veselkov04@imperial.ac.uk)

License: 

The MIT License (MIT)

Copyright 2019 Imperial College London

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

"""

#Loading global modules and dependencies
import pandas as pd;
import numpy as np;
import os;
import sys;

#Checking if the system is right and if the module is run as a script instead of being imported
if __name__== '__main__':
    if sys.byteorder!='little':
        print('Only little endian machines currently supported! bye bye ....');
        quit();
else:
    print('This module is not meant to be imported, but should be run as a script instead!')
    quit();
    
#Figuring out and adding the path to local modules    
module_path = os.path.dirname(os.path.realpath(__file__));
sys.path.append(module_path);

#Loading local modules
from printlog import start_log, stop_log, printlog;  #print and log printed messages in one go
from methodsmodule import load_full_profiles, remove_outliers, optimize_classifier, get_score_average, get_class_prob, save_predicted_probabilities;
#different functions used by the algorithm
from utils import load_global_settings; 

######################################################################################
#Initialization of the settings, paths and parameters, loading of data files
######################################################################################
settings_file = os.path.abspath(os.path.join(module_path, '../settings.ini'));
root_folder = os.path.dirname(settings_file);
global_settings = load_global_settings(settings_file);

logfile = os.path.join(global_settings['Default Paths']['Results_Path'], 'machine_learning.log');
start_log(logfile, autoclose = True, overwrite_existing = True, verbosity_level = 0);

method = global_settings['Machine Learning Settings']['Method'].lower();

basefile = '';
# 0 - for Linear SVM, 1 - for Radial SVM, 2 - for MMC
if method == 'radialsvm':
    method = 1;
    basefile += 'RadialSVM';
elif method == 'linearsvm':
    method = 0;
    basefile += 'LinearSVM';
else:
    method = 2;
    basefile += 'MMC';

# 0 - for Standard, 1 - for Log/Standard
if global_settings.getboolean('Machine Learning Settings', 'LogTransform'):
    prescaler = 1;
    basefile += '_LogTransformed';
else:
    prescaler = 0;
    basefile += '_Standard';

N_repeats = global_settings.getint('Machine Learning Settings', 'N_repeats');
cv_folds = global_settings.getint('Machine Learning Settings', 'CV_folds');
test_size = global_settings.getfloat('Machine Learning Settings', 'Test_size');

results_path = global_settings['Default Paths']['Results_Path'];
interactome_path = global_settings['Default Paths']['Interactome_Path'];
compounds_path = global_settings['Default Paths']['Compounds_Path'];
scratch_path = global_settings['Default Paths']['Scratch_Path'];


    
#####################################################################################
#Loading main data
#####################################################################################

#Genes first
fname = os.path.join(interactome_path, 'genes.csv.gz');
printlog('Genes source file:');
printlog(fname);
printlog('Reading genes...');
genes = pd.read_csv(fname, index_col = 0);
gene_count = len(genes.index);
printlog('%s genes loaded.'%gene_count);

#Now compounds...
fname = os.path.join(scratch_path, 'ids.csv');
printlog('Compounds combined info file:');
printlog(fname);
printlog('Reading compounds...');
compounds = pd.read_csv(fname, index_col = 0);
compound_count = len(compounds.index);
printlog('%s compounds loaded.'%compound_count);

#And propagated vectors....
fname = os.path.join(scratch_path, 'vectors.dat');
printlog('Vectors file:');
printlog(fname);
printlog('Reading propagation vectors...');
vectors = np.fromfile(fname,dtype = np.float32).reshape(-1, gene_count);
vectors_count = vectors.shape[0];
printlog('%s vectors loaded.'%vectors_count);


#Prepare data
X_train, y_train, train_ids, train_names, train_DBIDs, train_InChIKeys,\
X_test, test_ids, test_names, test_DBIDs, test_InChIKeys = load_full_profiles(compounds, vectors, prescaler == 1);

printlog('Train samples: %s'%len(X_train));
printlog('Test samples: %s'%len(X_test));

printlog('N_pos: %s N_neg: %s'%(np.sum(y_train == 1), np.sum(y_train == 0)));

printlog('Min train vect: %s'%np.min(X_train))   
printlog('Max train vect: %s'%np.max(X_train))   

printlog('Min test vect: %s'%np.min(X_test))   
printlog('Max test vect: %s'%np.max(X_test))   


#Optionally filter out outliers and make outlier report
if global_settings.getboolean('Machine Learning Settings', 'Remove_Outliers'):
    X_train, y_train, train_ids, train_names, train_DBIDs, train_InChIKeys,\
    X_test, test_ids, test_names, test_DBIDs, test_InChIKeys = \
                                remove_outliers(results_path, basefile, 
                                             X_train, y_train, train_ids, train_names, train_DBIDs, train_InChIKeys,
                                             X_test, test_ids, test_names, test_DBIDs, test_InChIKeys);

#This is where the main work will be done, i.e. Stratified Kfold analysis, model building, internal cross-validation based
#parameter optimization etc.
optimal_classifier, p_train, gene_scores = optimize_classifier(X_train, y_train, method, N_repeats, cv_folds, test_size = test_size,
                                             report_file = os.path.join(results_path, basefile + "_params_optimization.csv"));
    
with open(os.path.join(results_path, basefile + "_params_optimization.csv"), 'w') as fout:
    fout.write('post_f_score: %.2f (%.2f)\n'%get_score_average(optimal_classifier.cv_results_, 'balance'));
    fout.write('post_ac_score: %.2f (%.2f)\n'%get_score_average(optimal_classifier.cv_results_, 'ac_correct'));
    fout.write('post_nac_score: %.2f (%.2f)\n'%get_score_average(optimal_classifier.cv_results_, 'nac_correct'));
    fout.write('post_logratio: %.2f (%.2f)\n'%get_score_average(optimal_classifier.cv_results_, 'logratio'));

printlog('Predicting probabilities....')
 
p_test = get_class_prob(optimal_classifier, optimal_classifier.predict_proba(X_test), 1);
    
printlog('Saving results...')

save_predicted_probabilities(os.path.join(results_path, basefile + "_prob_train.csv"), p_train, train_ids, train_names, train_DBIDs, train_InChIKeys);
save_predicted_probabilities(os.path.join(results_path, basefile + "_prob_test.csv"), p_test, test_ids, test_names, test_DBIDs, test_InChIKeys);

with open(os.path.join(results_path, basefile + "_gene_scores.csv"), 'w') as fout:
    fout.write('Index,Gene_Score,Gene_Name,Ensembl_G,Ensembl_P,GSEA\n');
    for i in range(gene_count):
        fout.write(','.join([
                '%s'%i,
                '%s'%np.real(gene_scores[i])[0],
                '"%s"'%genes['Name'][i],
                '"%s"'%genes['Ensembl_G'][i],
                '"%s"'%genes['Ensembl_P'][i],
                '"%s"'%genes['GSEA'][i],
                ]))
        fout.write('\n');
        
if global_settings.getboolean('Cleanup', 'cleanup_enabled'):
    printlog('Cleaning up...');
    os.remove(os.path.join(scratch_path, 'ids.csv'))
    os.remove(os.path.join(scratch_path, 'vectors.dat'))


printlog('Processing completed. See %s folder for results.'%results_path);

stop_log();
